<?php
use yii\helpers\Html;
use yii\widgets\Menu;
?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                Action List
            </div>
            <?php echo Menu::widget([
                 'options' => ['class' => 'list-group', 'tag'=>'div'],
                 'items' => [
                        ['label' => 'List Devices', 'url' => [$cname.'/index'], 'visible' => !Yii::$app->user->isGuest],
                        ['label' => 'Summary', 'url' => [$cname.'/summary'], 'visible' => !Yii::$app->user->isGuest],
                        ['label' => 'Unused Devices', 'url' => [$cname.'/unused'], 'visible' => !Yii::$app->user->isGuest],
                        /*['label' => 'Upload Unused Devices', 'url' => [$cname.'/uploadunused'], 'visible' => !Yii::$app->user->isGuest &&Yii::$app->user->identity->is_admin()],*/
                        ['label' => 'Device Status', 'url' => [$cname.'/update'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin()],
                        ['label' => 'Bulk Update', 'url' => [$cname.'/bulkupdate'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin()],
                        ['label' => 'Device update list', 'url' => [$cname.'/updatelist'], 'visible' => !Yii::$app->user->isGuest],
                       /* ['label' => 'Upload Device Data', 'url' => [$cname.'/upload'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin()],*/
                        ['label' => 'Append Devices', 'url' => [$cname.'/append'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin()],
                        ['label' => 'History of Updates', 'url' => [$cname.'/updatehistory'], 'visible' => !Yii::$app->user->isGuest],
						//['label' => 'Invoice', 'url' => [$cname.'/updatehistory'], 'visible' => !Yii::$app->user->isGuest]

                 ],
                 'linkTemplate' => '<a href="{url}" style="display:block;"  >{label}</a>',
                 'activeCssClass' => 'list-group-item-info',
                'itemOptions'=>['tag'=>'div', 'class'=>"list-group-item "]
            ]);
            ?>
        </div>