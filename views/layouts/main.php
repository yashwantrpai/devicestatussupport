<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'SievaNetworks',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse',
        ],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'activateParents'=>'true',
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
           // (!Yii::$app->user->isGuest && (Yii::$app->user->identity->role == 'developer'))?(
            ['label' => 'SpyTec', 'url' => ['/spytec/index'], 'visible'=>!Yii::$app->user->isGuest ],
            ['label' => 'Status', 'url' => ['status/index'], 'visible' =>!Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin()],
            ['label' => 'Plans', 'url' => ['plans/index'], 'visible' =>!Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin()],
             ['label' => 'Update Type', 'url' => ['updatetype/index'], 'visible' =>!Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin()],
              ['label' => 'Device Type', 'url' => ['devicetype/index'], 'visible' =>!Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin()],
           //   ['label' => 'Sim Login', 'url' => ['simlogin/index'], 'visible' =>!Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin()],
             ['label' => 'Users', 'url' => ['users/index'], 'visible' => !Yii::$app->user->isGuest && Yii::$app->user->identity->is_admin()],
             ['label' => 'Log', 'url' => ['userloghistory/index'], 'visible' => !Yii::$app->user->isGuest],
          //  ):(
         //       ['label' => 'Updates', 'url' => ['/updates/index']]
          //  ),
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container-fluid">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; SievaNetworks <?= date('Y') ?></p>


    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
