<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Devicetype */

$this->title = 'Update Devicetype: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Devicetypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="devicetype-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
