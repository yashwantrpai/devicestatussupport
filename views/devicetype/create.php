<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Devicetype */

$this->title = 'Create Devicetype';
$this->params['breadcrumbs'][] = ['label' => 'Devicetypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="devicetype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
