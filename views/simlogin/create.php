<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Simlogin */

$this->title = 'Create Simlogin';
$this->params['breadcrumbs'][] = ['label' => 'Simlogins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="simlogin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
