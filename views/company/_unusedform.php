<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use app\models\Status;
use app\models\Devicetype;

/* @var $this yii\web\View */
/* @var $model app\models\Unused */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="unused-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'imei')->textInput() ?>

    <?= $form->field($model, 'sim')->textInput() ?>

    <?= $form->field($model, 'phone')->textInput() ?>
    
    <?php
		$items = ArrayHelper::map(Devicetype::find() -> all(), 'id', 'device');
		echo $form -> field($model, 'devicetypeid') -> dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Device Type']);
		?>

    <?php echo $form->field($model, 'incdate')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99-99-9999','options' => ['placeholder' => 'MM-DD-YYYY','class' => 'form-control',]]);?>

    <?= $form->field($model, 'particulars')->textarea(['rows' => 3]) ?>

    <?php $items = ArrayHelper::map(Status::find()->all(), 'id', 'status');
    echo $form->field($model, 'status')->dropDownList($items); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
