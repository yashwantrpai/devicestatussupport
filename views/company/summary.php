<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Unused */

$this->title = 'Summary : '.$cname;
$this -> params['breadcrumbs'][] = ['label' => $cname, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-lg-2">
	     <?= $this->render('/layouts/_menu', [
        'cname' => $cname,
    ]) ?>

	</div>
	<div class="col-lg-10 summary-view">

	    <h1><?= Html::encode($this->title) ?></h1>
	<?php /*	<div class="panel panel-default">
		  <div class="panel-body">
		    <h3>Total devices:<span class="pull-right label label-info"><?=$totaldevices ?></span></h3>
		  </div>
		</div>
		
		<div class="panel panel-default">
		  <div class="panel-heading"><h3>Devices as per <strong>Status</strong></h3></div>
		  <div class="panel-body">
		    	<?php
		    	foreach ($statuswiseImei as $key => $value) {
		    	?>
		    	<div class="col-lg-7"><?=$key ?></div><div class="col-lg-5 text-right"><span class="label label-info"><?=$value ?></span></div>
		    	<?php
		    	}
				?>
		  </div>
		</div>
		<div class="panel panel-default">
		  <div class="panel-heading"><h3>Devices as per <strong>Plan</strong></h3></div>
		  <div class="panel-body">		    
		    	<?php
		    	foreach ($planwiseImei as $key => $value) {
		    	?>
		    	<div class="col-lg-7"><?=$key ?></div><div class="col-lg-5 text-right"><span class="label label-info"><?=$value ?></span></div>
		    	<?php
		    	}
				?>
		  </div>
		</div>
	 * 
	 * 
	 
	 		
		<div class="panel panel-default">
		  <div class="panel-body">
		    <h3>Unused devices:<span class="pull-right label label-info"><?=$totalunused ?></span></h3>
		  </div>
		</div>
*/	?>	
		<div class="panel panel-default">
		  <div class="panel-heading h3">Active Devices as per <strong>Plan</strong> </div>
		  <div class="panel-body">
		  	<div class="col-lg-7"><h4><?=$plancount['4']['name'] ?></h4></div><div class="col-lg-5 text-right"><span class="label label-info"><?=$plancount['4']['count'] ?></span></div>
		  	<div class="col-lg-7"><h4><?=$plancount['2']['name'] ?></h4></div><div class="col-lg-5 text-right"><span class="label label-info"><?=$plancount['2']['count'] ?></span></div>
		  	<div class="col-lg-7"><h4><?=$plancount['1']['name'] ?></h4></div><div class="col-lg-5 text-right"><span class="label label-info"><?=$plancount['1']['count'] ?></span></div>
		  	<div class="col-lg-7"><h4><?=$plancount['3']['name'] ?></h4></div><div class="col-lg-5 text-right"><span class="label label-info"><?=$plancount['3']['count'] ?></span></div>
		    <?php
/*
		    	
		    	print_r($plancount['4']);
		    	foreach ($plancount as $key => $value) {
		    	?>
		    	<div class="col-lg-7"><h4><?=$value['name'] ?></h4></div><div class="col-lg-5 text-right"><span class="label label-info"><?=$value['count'] ?></span></div>
		    	<?php
		    	}
				*/ ?>
		  </div>
		</div>
	</div>
</div>
