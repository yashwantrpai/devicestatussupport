<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use app\models\Status;
use app\models\Plans;
use app\models\Updatetype;
use app\models\Updates;
use app\models\Devicetype;
//use app\models\Simlogin;
use app\models\History;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\daterange\DateRangePicker;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\StatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'Device Status';
$this->params['breadcrumbs'][] = ['label' => $cname, 'url' => ['index']];
$this -> params['breadcrumbs'][] = $this -> title;

$this->registerJs("$('.historybutton').click(showhistory);", \yii\web\View::POS_READY);
$this->registerJs("$('.updatesbutton').click(updates);", \yii\web\View::POS_READY);
?>

<div class="row">
    <?php
        $umodel = new Updates();
        Modal::begin([
        'header' => '<h2>Update Status</h2>',
        'id' => 'updatemodal',
        'size'=>Modal::SIZE_LARGE,
        ]);

        ?>
        <div id='lastentry'>Last Update : </div>       
        
        <?php $form = ActiveForm::begin(['action' => ['update']]); ?>
        
        <div class="col-md-6">
        
        <?= $form->field($umodel, 'imei')->textInput(['maxlength' => true, 'value'=>'', 'id'=>'imei']) ?>
        <?= $form->field($umodel, 'sim')->textInput(['maxlength' => true, 'value'=>'', 'id'=>'sim']) ?>
        <?= $form->field($umodel, 'phone')->textInput(['maxlength' => true, 'value'=>'', 'id'=>'phone']) ?>
        <?php
            $items = ArrayHelper::map(Status::find()->all(), 'id', 'status');
            echo $form->field($umodel, 'statusid')->dropDownList($items);
        ?>
        <?php
            $items = ArrayHelper::map(Plans::find()->all(), 'planid', 'plan');
            echo $form->field($umodel, 'planid')->dropDownList($items);
        ?>
        <?php
		$items = ArrayHelper::map(Updatetype::find() -> all(), 'id', 'type');
		echo $form -> field($umodel, 'updatetypeid') -> dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Update Type']);
		?>
        </div>
        <div class="col-md-6">
        <div>
        	<span><b>Last Particulars:</b></span>
        	<span id="lastparticulars"></span>
        	<br />
        </div>
         <?= $form->field($umodel, 'particulars')->textarea(['rows' => 3]) ?>
         
        <?= $form->field($umodel, 'updateon')->widget(DatePicker::classname(), [
		    'options' => ['placeholder' => 'Select update date ...'],
		    'pluginOptions' => [
		        'format' => 'mm/dd/yyyy',
		        'todayHighlight' => true
		    ]
		]) ?>

       

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
        </div>
    <?php ActiveForm::end(); ?>
    <div class="clearfix"></div>
    <?php
        Modal::end();
      ?>

        <?php  // Modal to show history
            Modal::begin([
                'header' => '<h2>Device Status History</h2>',
                'id'=>'historymodal',
                ]);
            ?>
            <div id="historyTabular">
                <table id="tblhistory" class="table">
                </table>
            </div>
            <?php
            Modal::end();
        ?>
    <div class="col-lg-2">
         <?= $this -> render('/layouts/_menu', ['cname' => $cname, ]) ?>

    </div>
    <div class="col-lg-10 update-index">
			<h1><?= Html::encode($this -> title) ?></h1>
            <div class="imei-search">

            <?php /* $form = ActiveForm::begin(['action' => ['update'], 'method' => 'get', ]); ?>

            <?= $form -> field($searchModel, 'imei') ?>

            <div class="form-group">
                <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end();  */ ?>

        </div>
<?php 
if(Yii::$app->user->identity->username=='sapna'|| Yii::$app->user->identity->role=='admin'){
	?>
    <p>
    	
        <?= Html::a('Export Total - ODS', [$cname . '/exportall'], ['class' => 'btn btn-success', 'target'=>'_blank']) ?>
    </p>
    
    <?php
}
?>
     <?php
	 
		//$count = Yii::$app->db->createCommand('SELECT COUNT(*) FROM devicestatus_spytec.imeidata')->queryScalar();
		// echo $count;
		
		//$dataProvider = new SqlDataProvider([
			//'sql' => 'SELECT * FROM devicestatus_spytec.imeidata',
			//'totalCount' => $count,
			//'pagination' => [
				//'pageSize' => 100,
			//],
		//]);
	 
            $gridColumns = [
                'id',
                'imei',
                'sim',
               /* ['attribute' => 'simlogin',
                	'filter'=>ArrayHelper::map(Simlogin::find()->asArray()->all(), 'id', 'name'),
                	'visible'=> Yii::$app->user->identity->username!='spytec',
	                'value' => function($model) {
	                	$provider = new Simlogin();
	                    return $provider->showProvider($model['simlogin']);
	                    //return implode(" ",$items);
	                }
	            ],*/
                'phone',
                ['attribute' => 'devicetypeid',
                	'filter'=>ArrayHelper::map(Devicetype::find()->asArray()->all(), 'id', 'device'),
	                'value' => function($model) {
	                	$device = new Devicetype();
	                    return $device->showDevice($model['devicetypeid']);
	                    //return implode(" ",$items);
	                }
	            ],
	            [
	               'filter'=>ArrayHelper::map(Updatetype::find()->asArray()->all(), 'id', 'type'),
                   'label' => 'Update Type',
                   'value' => function ($model) {
                       $history = new History();
                       $id = $history->lastupdate($model->imei);
                       $updateype = new Updatetype();
                       return $updateype->showUpdatetype($id);
                   }
	            ],
                ['attribute'=>'status',
					'filter'=>ArrayHelper::map(Status::find()->asArray()->all(), 'status', 'status'),
                ],
                //'planid',
                ['attribute' => 'planid',
                	'filter'=>ArrayHelper::map(Plans::find()->asArray()->all(), 'planid', 'plan'),
	                'value' => function($model) {
	                	$plan = new Plans();
	                    return $plan->showPlan($model['planid']);
	                    //return implode(" ",$items);
	                }
	            ],
                ['attribute' => 'particulars',
					'format'=>'html',
					'value' => function($model){
						$particulars_split = explode(";", $model['particulars']);
						$particulars_formatted = "";
						for($i = 0; $i < count($particulars_split) - 1; $i++){
							$particulars_formatted = $particulars_formatted."<b>".(formatted_date($particulars_split[$i])."</b>&nbsp;&nbsp;".$particulars_split[$i])."<br><br>";
						}
						return $particulars_formatted;
					}
				],
                //'incdate',
                [
                	'attribute' => 'incdate',
                	//'format' => 'datetime',/**/
                    'filter' => DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'incdate',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'Y-m-d',
                            ],
                        ],
                    ]),
                    'value' => function ($model) {
                		$time = strtotime($model->incdate);
                		return date('m/d/Y',$time);
					},
                ],
                //'date',
                [
                	'attribute' => 'date',
                	//'format' => 'datetime',
                    'filter' => DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'Y-m-d',
                            ],
                        ],
                    ]),
                    'value' => function ($model) {
                		$time = strtotime($model->date);
                		return date('m/d/Y',$time);
					},
                ],
                ['class' => 'yii\grid\ActionColumn','template' => '{update} {history} {lastupdate}','visibleButtons'=>['update'=>Yii::$app->user->identity->username=='sapna'] ,'buttons'=>['history'=> function ($url, $model) { return Html::button( '<span class="glyphicon glyphicon-file"> </span>', ['value'=>$model['imei'],'data-value'=>$model['imei'], 'data-url'=>$url, 'class' => 'btn btn-default btn-xs historybutton', 'title'=>'History']);}, 'lastupdate'=> function ($url, $model) { return Html::a( '<span class="glyphicon glyphicon-bookmark"> </span>', '#', ['data-value'=>$model['imei'], 'data-url'=>$url, 'class' => 'updatesbutton', 'title'=>'Update Status']);},]],
            ];
			
			//print_r($dataProvider);
			
			//echo GridView::widget([
					//'dataProvider' => $dataProvider,
					//'columns' => $gridColumns
				//]);
			
    	  echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
                'fontAwesome' => true,
                'timeout' => 0,
                'stream' => true, // this will automatically save the file to a folder on web server
                'streamAfterSave' => true, // this will stream the file to browser after its saved on the web folder
                'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser,
                'target' => '_blank',
                'dropdownOptions' => [
                    'label' => 'Export All',
                    'class' => 'btn btn-default'
                ],
            ]) . " <hr>\n".
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
                'pager' => [
			        'firstPageLabel' => 'First',
			        'lastPageLabel' => 'Last',
			    ],
            ]);     
?>

<?php
	function formatted_date($particulars){
		$date = (date_parse($particulars));
			if(($date['year'] != "") && ($date['month'] != "") && ($date['day'] != "")){
				$date = $date['year']."/".$date['month']."/".$date['day'];
				$date = date_create($date);
				return date_format($date, "m/d/Y");
			}else {
				return "NA";
			}
	}
?>
    </div>
</div>