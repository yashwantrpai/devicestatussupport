/**
 * @author Bhaskar
 */

function showCreateEvent(){
	$('#eventmodal').modal();
}

function showhistory(){
	str = $(this).data('value');
   $.ajax({
      url: $(this).data('url')+'&imei='+str,
	  method: "GET",
	  dataType: "json"
	}).done(function( msg ) {
	    if(msg == "False"){
	        alert("No historical data for this device.");
	        return;
	    }
		$('#tblhistory').html('<tr><th>Status</th><th>Plans</th><th>Particulars</th><th>Date</th><th>User</th><th>Update Type</th></tr>');
	  	$.each(msg, function(i, item) {
        	$('<tr>').append(
	            $('<td>').text(item.newstatus),
	            $('<td>').text(item.newplan),
	            $('<td>').text(item.newparticulars),
	            $('<td>').text(item.date),
	            $('<td>').text(item.username),
	            $('<td>').text(item.updatetypeid)
        	).appendTo('#tblhistory');
    	});
	  	$('#historymodal').modal();
	}).fail(function( jqXHR, textStatus ) {
	  alert( "Request failed: " + jqXHR.responseJSON.message);
	  return;
	});

}

function duplicates(){
		   
	$.ajax({
      url: 'index.php?r=spytec/duplicateupdates',
	  method: "GET",
	  dataType: "json"
	}).done(function( msg ) {
		//var dups = JSON.parse(msg);
		var dups = msg;
		//alert(dups);
		for(var i = 0; i < dups.length; i++){	
			var imei = dups[i].imei;
			$('table tr').each(function(i){
				$("table tr").each(function(i) {
					var f = false;
					$('td', this).each(function (i) {
						if($(this).html() == imei){
							f = true;
						}
					});
					if(f){
						$(this).addClass('danger');
					}
				});
			});
		}		
	});
}

function updates(){
  str = $(this).data('value');
   $.ajax({
      url: $(this).data('url')+'&imei='+str,
      method: "GET",
      dataType: "json"
    }).done(function( msg ) {
        if(msg != 'False'){
            $("#lastentry").html("Last Updated on - "+msg.imeidata.date);
        }
        $('#imei').val(str);
        $('#imei').attr('readonly','true');
        
        $('#sim').val(msg.imeidata.sim);
        $('#phone').val(msg.imeidata.phone);

        $('#updates-statusid').val(msg.status_id);
        $('#updates-planid').val(msg.imeidata.planid);
        
        $('#lastparticulars').html(msg.imeidata.particulars);

        $("#updatemodal").modal();
    }).fail(function( jqXHR, textStatus ) {
      alert( "Request failed: " + jqXHR.responseJSON.message);
      return;
    });
}
