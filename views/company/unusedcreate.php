<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Unused */

$this->title = 'Create Unused';
$this -> params['breadcrumbs'][] = ['label' => $cname, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Unuseds', 'url' => ['unused']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
	<div class="col-lg-2">
	     <?= $this->render('/layouts/_menu', [
        'cname' => $cname,
    ]) ?>

	</div>
	<div class="col-lg-7 unused-create">

	    <h1><?= Html::encode($this->title) ?></h1>
	
	    <?= $this->render('_unusedform', [
	        'model' => $model,
	    ]) ?>

	</div>
</div>