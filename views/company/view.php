<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Updates */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => $cname, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Device Update List', 'url' => ['updatelist']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-2">
         <?= $this->render('/layouts/_menu', [
        'cname' => $cname,
    ]) ?>

    </div>
<div class="updates-view col-lg-10">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['modify', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['deleteupdate', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'imei',
            'statusid',
            'particulars:ntext',
            'updateon',
            'datetime',
            'userid',
            'ratified',
            'comments:ntext',
        ],
    ]) ?>

</div>
</div>