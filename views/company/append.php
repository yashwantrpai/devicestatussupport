<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

use app\models\Imeidata;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = 'Append Devices';
$this->params['breadcrumbs'][] = ['label' => $cname, 'url' => ['index']];
$this -> params['breadcrumbs'][] = $this -> title;

$this->registerJs("$('#render').click(function(){\$(this).hide();});", \yii\web\View::POS_READY);
?>
<div class="row">

    <div class="col-lg-2">
         <?= $this->render('/layouts/_menu', [
        'cname' => $cname,
    ]) ?>

    </div>
    <div class="col-lg-10">
        <?php if($msg){ // print_r($filedata); ?>
        	
                <div class="alert alert-success" role="alert">File Uploaded Successfully. <?= Html::a('Download File', Url::to('/upload/'.$filedata->imageFile->name), ['class' => 'btn btn-success']) ?> Render to Database: <?= Html::a('Render', ['renderappend', 'path' => $path], ['class' => 'btn btn-danger', 'id'=>'render']) ?></div>
       <?php } ?>
        <span class="h3"> Upload Excel file - </span><br /><span class="h5">Please make sure the file is is ODS (Open Document Spreadsheet) format.</span><br /><br />
        <?php $form = ActiveForm::begin(['action' => [$cname.'/append'], 'options' => ['enctype' => 'multipart/form-data']]); ?>

        <?= $form->field($filedata, 'imageFile')->fileInput()->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
    </div>
    <div class="col-lg-10">
        <?php $model =  new Imeidata();
            echo $this->render('create', ['model' => $model, 'cname'=>$cname]);
        ?>
    </div>
</div>
