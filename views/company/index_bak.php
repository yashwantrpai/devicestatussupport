<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = $cname;
$this -> params['breadcrumbs'][] = $this -> title;

//print_r(Yii::$app->user->identity);
?>

<div class="row">
	<div class="col-lg-2">
	     <?= $this->render('/layouts/_menu', [
        'cname' => $cname,
    ]) ?>

	</div>
	<div class="col-lg-10">
        <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

             // 'id',
            'imei',
            'sim',
            'phone',
            'status',
            'particulars:ntext',
            'date',

           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	</div>
</div>
