<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Status;
use app\models\Plans;
use app\models\Updatetype;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Updates */

$this->title = 'Update Device Status: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => $cname, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Device Update List', 'url' => ['updatelist']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-lg-2">
         <?= $this -> render('/layouts/_menu', ['cname' => $cname, ]) ?>

    </div>
    <div class="updates-update col-lg-10">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="updates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'imei')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'sim')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?php $items = ArrayHelper::map(Status::find()->all(), 'id', 'status');
    echo $form->field($model, 'statusid')->dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Status']); ?>
    
    <?php $items = ArrayHelper::map(Plans::find()->all(), 'planid', 'plan');
    echo $form->field($model, 'planid')->dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Plan']); ?>
    
    <?php
		$items = ArrayHelper::map(Updatetype::find() -> all(), 'id', 'type');
		echo $form -> field($model, 'updatetypeid') -> dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Update Type']);
		?>

    <?= $form->field($model, 'particulars')->textarea(['rows' => 3]) ?>
    
    <?= $form->field($model, 'updateon')->widget(DatePicker::classname(), [
		    'options' => ['placeholder' => 'Select update date ...'],
		    'pluginOptions' => [
		        'format' => 'mm/dd/yyyy',
		        'todayHighlight' => true
		    ]
		]) ?>

    <?= $form->field($model, 'datetime')->textInput() ?>

    <?= $form->field($model, 'userid')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'ratified')->dropDownList(['0'=>"Pending", '1'=>"Confirmed",]) ?>

    <?= $form->field($model, 'comments')->textarea(['rows' => 3]) ?>

    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
</div>
