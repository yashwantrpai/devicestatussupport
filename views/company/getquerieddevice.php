<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;

$this->title = 'Device Status History';

?>
<div class="row">
	<div class="col-lg-2">
	
	</div>
	<div class="col-lg-8">
		
		<center><h1><?= Html::encode($this->title) ?></h1>
		<h2><b>Device Status History for date: <?= $date ?></b></h2>
		<?php //print_r($dataProvider); ?>
		
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			]);
		?>
		</center>	
		
	</div>
</div>