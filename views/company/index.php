<?php
use yii\helpers\Html;
//use yii\grid\GridView;
use yii\bootstrap\Modal;
use kartik\export\ExportMenu;
use kartik\grid\GridView;
use kartik\daterange\DateRangePicker;
use app\models\Status;
use app\models\Plans;
use app\models\Devicetype;
//use app\models\Simlogin;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this -> title = ucfirst($cname).' :: Device List';
$this -> params['breadcrumbs'][] = $this -> title;
if(isset($this->duplicates)){
    echo '<pre>';
    print_r($this->duplicates);
    echo '</pre>';
}

$this->registerJs("$('.historybutton').click(showhistory);", \yii\web\View::POS_READY);
//print_r(Yii::$app->user->identity);
?>

 		<?php  // Modal to show history
            Modal::begin([
                'header' => '<h2>Device Status History</h2>',
                'id'=>'historymodal',
                ]);
            ?>
            <div id="historyTabular">
                <table id="tblhistory" class="table">
                </table>
            </div>
            <?php
            Modal::end();
        ?>

<div class="row">
	<div class="col-lg-2">
	     <?= $this->render('/layouts/_menu', [
        'cname' => $cname,
    ]) ?>

	</div>
	<div class="col-lg-10">
		<h1><?= Html::encode($this -> title) ?></h1>
		
	    <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn'],

                 // 'id',
                'imei',
                //'sim',
                ['attribute' => 'sim', 'visible'=> Yii::$app->user->identity->username!='spytec'],
                /*['attribute' => 'simlogin',
                	'filter'=>ArrayHelper::map(Simlogin::find()->asArray()->all(), 'id', 'name'),
                	'visible'=> Yii::$app->user->identity->username!='spytec',
	                'value' => function($model) {
	                	$provider = new Simlogin();
	                    return $provider->showProvider($model['simlogin']);
	                    //return implode(" ",$items);
	                }
	            ],*/
                ['attribute' => 'phone', 'visible'=> Yii::$app->user->identity->username!='spytec'],
                ['attribute' => 'devicetypeid',
                	'filter'=>ArrayHelper::map(Devicetype::find()->asArray()->all(), 'id', 'device'),
	                'value' => function($model) {
	                	$device = new Devicetype();
	                    return $device->showDevice($model['devicetypeid']);
	                    //return implode(" ",$items);
	                }
	            ],
                ['attribute'=>'status',
                'filter'=>ArrayHelper::map(Status::find()->asArray()->all(), 'status', 'status'),
                ],
                //'planid',
                ['attribute' => 'planid',
                	'filter'=>ArrayHelper::map(Plans::find()->asArray()->all(), 'planid', 'plan'),
	                'value' => function($model) {
	                	$plan = new Plans();
	                    return $plan->showPlan($model['planid']);
	                    //return implode(" ",$items);
	                }
	            ],
                'particulars:ntext',
                //'incdate',
                [
                	'attribute' => 'incdate',
                	//'format' => 'datetime',/**/
                    'filter' => DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'incdate',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'Y-m-d',
                            ],
                        ],
                    ]),
                    'value' => function ($model) {
                		$time = strtotime($model->incdate);
                		return date('m/d/Y',$time);
					},
                ],
                //'date',
                [
                	'attribute' => 'date',
                	//'format' => 'datetime',
                    'filter' => DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'date',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'Y-m-d',
                            ],
                        ],
                    ]),
                    'value' => function ($model) {
                		$time = strtotime($model->date);
                		return date('m/d/Y',$time);
					},
                ],

               ['class' => 'yii\grid\ActionColumn','template' => '{history}', 'buttons'=>['history'=> function ($url, $model) { return Html::button( '<span class="glyphicon glyphicon-file"> </span>', ['value'=>$model['imei'],'data-value'=>$model['imei'], 'data-url'=>$url, 'class' => 'btn btn-default btn-xs historybutton', 'title'=>'History']);}]],
            ];
            // Renders a export dropdown menu

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
                'fontAwesome' => true,
                'timeout' => 0,
                'stream' => true, // this will automatically save the file to a folder on web server
                'streamAfterSave' => true, // this will stream the file to browser after its saved on the web folder
                'deleteAfterSave' => true, // this will delete the saved web file after it is streamed to browser,
                'target' => '_blank',
                'dropdownOptions' => [
                    'label' => 'Export All',
                    'class' => 'btn btn-default'
                ],
            ]) . " <hr>\n".
            GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns,
                'pager' => [
			        'firstPageLabel' => 'First',
			        'lastPageLabel' => 'Last',
			    ],
            ]);
			
			//(Presently export over 20k data is not possible. Please filter and then export.)
        ?>
        <?php /* = GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumns,
        ]); */ ?>


	</div>
</div>
