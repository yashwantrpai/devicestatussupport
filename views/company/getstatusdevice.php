<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

$this->title = 'Get Status of Device';
/* @var $this yii\web\View */
/* @var $model app\models\Updates */
/* @var $form yii\widgets\ActiveForm */
?>
	<div class="col-lg-2">
	
	</div>
<div class="updates-create col-lg-8">
	<?php $form = ActiveForm::begin([
		'action' => ['company/getquerieddevice'],
		'method' => 'post',
	]); ?>

    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
		    'options' => ['placeholder' => 'Select date..'],
		    'pluginOptions' => [
		        'format' => 'yyyy-mm-dd',
		        'todayHighlight' => true
		    ]
		]) ?>
		
	<?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'value'=>'submit', 'name'=>'submit']); ?>

    <?php ActiveForm::end(); ?>
	

</div>