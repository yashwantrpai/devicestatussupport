<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

use yii\helpers\ArrayHelper;
use app\models\Status;
use app\models\Plans;
use app\models\Devicetype;
use kartik\date\DatePicker;
//use app\models\Simlogin;

/* @var $this yii\web\View */
/* @var $model app\models\Imeidata */
/* @var $form ActiveForm */
?>
<hr />
<div class="h3">Add a single Device:</div>
<div class="company-create col-lg-7">

    <?php $form = ActiveForm::begin(['action' => [$cname.'/create'],]); ?>

        <?= $form->field($model, 'imei') ?>
        <?= $form->field($model, 'sim') ?>
        <?php /*
		$items = ArrayHelper::map(Simlogin::find() -> all(), 'id', 'name');
		echo $form -> field($model, 'simlogin') -> dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Sim Login']);
		*/?>
        <?= $form->field($model, 'phone') ?>
        <?php
		$items = ArrayHelper::map(Devicetype::find() -> all(), 'id', 'device');
		echo $form -> field($model, 'devicetypeid') -> dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Device Type']);
		?>
        <?php
            $items = ArrayHelper::map(Status::find()->all(), 'id', 'status');
            echo $form->field($model, 'status')->dropDownList($items);
        ?>
        <?php
            $items = ArrayHelper::map(Plans::find()->all(), 'planid', 'plan');
            echo $form->field($model, 'planid')->dropDownList($items);
        ?>        
        <?= $form->field($model, 'incdate')->widget(DatePicker::classname(), [
		    'options' => ['placeholder' => 'Select date ...'],
		    'pluginOptions' => [
		        'format' => 'mm/dd/yyyy',
		        'todayHighlight' => true
		    ]
		]);
		/*echo $form->field($model, 'incdate')->widget(\yii\jui\DatePicker::className(), [
		//'language' => 'ru',
		'dateFormat' => 'MM-DD-YYYY',
		]);*/
		//echo $form->field($model, 'incdate')->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99/99/9999','options' => ['placeholder' => 'MM-DD-YYYY','class' => 'form-control',]]);
        ?>
        <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
		    'options' => ['placeholder' => 'Select date ...'],
		    'pluginOptions' => [
		        'format' => 'mm/dd/yyyy',
		        'todayHighlight' => true
		    ]
		]);?>

        <!--<?= $form->field($model, 'particulars')->textarea(['rows' => 3]) ?>-->

        <div class="form-group">
            <?= Html::submitButton('Add', ['class' => 'btn btn-primary']) ?>
        </div>
		<?= $form->errorSummary($model); ?>
    <?php ActiveForm::end(); ?>

</div><!-- company-create -->
