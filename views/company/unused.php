<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;

use app\models\Status;
use app\models\Devicetype;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UnusedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Unuseds';
$this -> params['breadcrumbs'][] = ['label' => $cname, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-lg-2">
	     <?= $this->render('/layouts/_menu', [
        'cname' => $cname,
    ]) ?>

	</div>
	<div class="col-lg-10 unused-index">

	    <h1><?= Html::encode($this->title) ?></h1>
	    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	
	    <p>
	    	<?php /*
	    	if(Yii::$app->user->identity->role=='admin'){
	    		?>
	        <?= Html::a('Create Unused', ['unusedcreate'], ['class' => 'btn btn-success']) ?>
	        
	        <?php
			}*/
			?>
	    </p>
	
	    <?= GridView::widget([
	        'dataProvider' => $dataProvider,
	        'filterModel' => $searchModel,
	        'pager' => [
			        'firstPageLabel' => 'First',
			        'lastPageLabel' => 'Last',
			    ],
	        'columns' => [
	            ['class' => 'yii\grid\SerialColumn'],
	
	            //'id',
	            'imei',
	            'sim',
	            'phone',
	            ['attribute' => 'devicetypeid',
                	'filter'=>ArrayHelper::map(Devicetype::find()->asArray()->all(), 'id', 'device'),
	                'value' => function($model) {
	                	$device = new Devicetype();
	                    return $device->showDevice($model['devicetypeid']);
	                    //return implode(" ",$items);
	                }
	            ],
	            //'incdate',
                [
                	'attribute' => 'incdate',
                	//'format' => 'datetime',/**/
                    'filter' => DateRangePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'incdate',
                        'convertFormat' => true,
                        'pluginOptions' => [
                            'locale' => [
                                'format' => 'Y-m-d',
                            ],
                        ],
                    ]),
                    'value' => function ($model) {
                		$time = strtotime($model->incdate);
                		return date('m/d/Y',$time);
					},
                ],
	            'particulars',
	           /* ['filter'=>ArrayHelper::map(Status::find()->asArray()->all(), 'id', 'status'),
	            'attribute' => 'status',
	                'value' => function($model) {
	                    return $model->getStatus($model->status);
	                    //return implode(" ",$items);
	                }
            	],
	*/
	        /*    ['class' => 'yii\grid\ActionColumn',
	             'visible'=>Yii::$app->user->identity->is_admin(),
	             'template' => '{view} {update} {delete}',
	             'buttons'  => [
			        'view' => function($url, $model) {
			                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
			                        'title' => Yii::t('app', 'View'),]);
			        },
			        'update' => function($url, $model) {
			                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
			                        'title' => Yii::t('app', 'Update'),]);
			        },
			        'delete' => function($url, $model) {
			                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
			                        'title' => Yii::t('app', 'Delete'), ]);
			        }
			     ],	             
	             'urlCreator' => function ($action, $model, $key, $index) {
		            if ($action === 'view') {
		                $url ='index.php?r='.$this->context->companyname.'/unusedview&id='.$model->id;
		                return $url;
		            }
		
		            if ($action === 'update') {
		                $url ='index.php?r='.$this->context->companyname.'/unusedupdate&id='.$model->id;
		                return $url;
		            }
		            if ($action === 'delete') {
		                $url ='index.php?r='.$this->context->companyname.'/unuseddelete&id='.$model->id;
		                return $url;
		            }
		
		          }
	            ],*/
	        ],
	    ]); ?>

	</div>
</div>