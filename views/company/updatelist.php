<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use app\models\Status;
use app\models\Plans;
use app\models\Updatetype;

use kartik\daterange\DateRangePicker;


$this -> title = 'Device update list';
$this->params['breadcrumbs'][] = ['label' => $cname, 'url' => ['index']];
$this -> params['breadcrumbs'][] = $this -> title;

$this->registerJs("$('.historybutton').click(showhistory);", \yii\web\View::POS_READY);
?>
<script>
	window.onload = function() {
		duplicates();
	};
</script>
        <?php  // Modal to show history
            Modal::begin([
                'header' => '<h2>Device Status History</h2>',
                'id'=>'historymodal',
                ]);
        ?>
            <div id="historyTabular">
                <table id="tblhistory" class="table">
                </table>
            </div>
        <?php
            Modal::end();
        ?>
<div class="row">
    <div class="col-lg-2">
         <?= $this -> render('/layouts/_menu', ['cname' => $cname, ]) ?>
    </div>
    <div class="col-lg-10 list-index">
		<center><h1><b><?= Html::encode($this->title) ?></b></h1>
		<p>    	
			<?= Html::a('Export Total - ODS', [$cname . '/exportexcel'], ['class' => 'btn btn-success', 'target'=>'_blank']) ?>
		</p>	
        <?php
        if(Yii::$app->user->identity->is_admin()){
			echo Html::beginForm(['bulk'],'post'); ?>
        <div class="pull-right">
            <?=Html::dropDownList('action','',[''=>'Mark selected as: ','20'=>'Confirmed','30'=>'Delete'],['class'=>'dropdown',])?>
            <?=Html::submitButton('Submit', ['class' => 'btn btn-info',]);?>
        </div>

      <?php  } ?>
	  
        <?php echo GridView::widget([
		'id' => 'table',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
			        'firstPageLabel' => 'First',
			        'lastPageLabel' => 'Last',
			    ],
		'rowOptions' => function($model, $key, $index, $grid){ 		
			return ['id'=>'row'.$index];
		},		
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'imei',
            'sim',
            'phone',
            ['attribute' => 'updatetypeid',
            	'filter'=>ArrayHelper::map(Updatetype::find()->asArray()->all(), 'id', 'type'),
                'value' => function($model) {
                	$updatetype = new Updatetype();
                    return $updatetype->showUpdatetype($model->updatetypeid);
                }
            ],
           // 'statusid',
            ['attribute' => 'statusid',
            'filter'=>ArrayHelper::map(Status::find()->asArray()->all(), 'id', 'status'),
                'value' => function($model) {
                    return $model->getStatus($model->statusid);
                    //return implode(" ",$items);
                }
            ],
            ['attribute' => 'planid',
            	'filter'=>ArrayHelper::map(Plans::find()->asArray()->all(), 'planid', 'plan'),
                'value' => function($model) {
                	$plan = new Plans();
                    return $plan->showPlan($model->planid);
                    //return implode(" ",$items);
                }
            ],
            
            'particulars:ntext',
            [
            	'attribute' => 'updateon',
            	//'format' => 'datetime',
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'updateon',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d',
                        ],
                    ],
                ]),
               // 'value' => $model->updateon,
            ],
            'datetime',
            ['attribute' => 'userid',
                'value' => function($model) {
                    return $model->getUser($model->userid);
                    //return implode(" ",$items);
                }
            ],
            'ratified',
            //'comments:ntext',
            ['class' => 'yii\grid\CheckboxColumn'],
            ['class' => 'yii\grid\ActionColumn','template' => '{deleteupdate} {modify} {confirm} {history}', 'buttons'=>[
            'deleteupdate'=> function ($url, $model) {
                if(Yii::$app->user->identity->is_admin()){
                     return Html::a( '<span class="glyphicon glyphicon-trash"> </span>', $url, ['data-value'=>$model['imei'], 'title'=>'Delete', 'data-confirm' => 'This will delete the update for device.', 'data-method' => 'post']);
                }
            },
            'modify'=> function ($url, $model) {
                if(Yii::$app->user->identity->is_admin()){
                     return Html::a( '<span class="glyphicon glyphicon-edit"> </span>', $url, ['data-value'=>$model['imei'], 'title'=>'Modify']);
                }
            },
            'confirm'=> function ($url, $model) {
                if(Yii::$app->user->identity->is_admin()){
                    return Html::a( '<span class="glyphicon glyphicon-ok"> </span>', $url, ['data-value'=>$model['imei'], 'title'=>'Confirm']);
                }
            },
            'history'=> function ($url, $model) { return Html::button( '<span class="glyphicon glyphicon-file"> </span>', ['value'=>$model['imei'],'data-value'=>$model['imei'], 'data-url'=>$url, 'class' => 'btn btn-default btn-xs historybutton', 'title'=>'History']);}]],
        ],
    ]); ?>
     <?php
        if(Yii::$app->user->identity->is_admin()){
			echo Html::endForm();
    }?>
	</center>
    </div>
</div>
