<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\daterange\DateRangePicker;

use app\models\Plans;
use app\models\Status;
use app\models\Updatetype;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Update History';
$this->params['breadcrumbs'][] = ['label' => $cname, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">

    <div class="col-lg-2">
         <?= $this->render('/layouts/_menu', [
        'cname' => $cname,
    ]) ?>

    </div>
<div class="change-history-index col-lg-10">
	
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<p>
		<?php 
			// print_r(Yii::$app->request->queryParams);
			$imei = ''; 
			$date = ''; 
			$updateon = '';
			$updatetypeid = ''; 
                        $oldsim='';
                        $newsim='';
                        $oldphone='';
                        $newphone='';
			$oldstatus = '';
			$newstatus = '';
			$oldplan = '';
			$newplan = '';
			$oldparticulars = '';
			$newparticulars = '';
			$comments = '';
			$username = '';
				
			if(count(Yii::$app->request->queryParams) == 2){
				$imei = Yii::$app->request->queryParams['HistorySearch']['imei']; 
				$date = Yii::$app->request->queryParams['HistorySearch']['date']; 
				$updateon = Yii::$app->request->queryParams['HistorySearch']['updateon'];
				$updatetypeid = Yii::$app->request->queryParams['HistorySearch']['updatetypeid']; 
                                
                                $oldsim = Yii::$app->request->queryParams['HistorySearch']['oldsim'];
                                $newsim = Yii::$app->request->queryParams['HistorySearch']['newsim'];
                                $oldphone = Yii::$app->request->queryParams['HistorySearch']['oldphone'];
                                $newphone = Yii::$app->request->queryParams['HistorySearch']['newphone'];
                                
				$oldstatus = Yii::$app->request->queryParams['HistorySearch']['oldstatus'];
				$newstatus = Yii::$app->request->queryParams['HistorySearch']['newstatus'];
				$oldplan = Yii::$app->request->queryParams['HistorySearch']['oldplan'];
				$newplan = Yii::$app->request->queryParams['HistorySearch']['newplan'];
				$oldparticulars = Yii::$app->request->queryParams['HistorySearch']['oldparticulars'];
				$newparticulars = Yii::$app->request->queryParams['HistorySearch']['newparticulars'];
				$comments = Yii::$app->request->queryParams['HistorySearch']['comments'];
				$username = Yii::$app->request->queryParams['HistorySearch']['username'];
			}
			
			// $params = Yii::$app->request->queryParams['HistorySearch']; 
		?>	
		<?php echo Html::button('Export ', array('onclick' => 'js:document.location.href="index.php?r='.$cname.'/exportupdaterange"')); ?>
		<?php // echo Html::button('Test (DO NOT PRESS!)', array('onclick' => 'js:document.location.href="index.php?r='.$cname.'/exportupdate&imei='.$imei.'&date='.$date.'&updateon='.$updateon.'&updatetypeid='.$updatetypeid.'&oldstatus='.$oldstatus.'&newstatus='.$newstatus.'&oldplan='.$oldplan.'&newplan='.$newplan.'&oldparticulars='.$oldparticulars.'&newparticulars='.$newparticulars.'&comments='.$comments.'&username='.$username.'"')); ?>
	</p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
			        'firstPageLabel' => 'First',
			        'lastPageLabel' => 'Last',
			    ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

          //  'id',
            'imei',
            'oldsim',
            'newsim',
            'oldphone',
            'newphone',
            //'date',
            [
            	'attribute' => 'date',
            	//'format' => 'datetime',
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d',
                        ],
                    ],
                ]),
                'value' => function ($model) {
            		$time = strtotime($model->date);
            		return date('m/d/Y',$time);
				},
            ],
           // 'updateon',
           [
            	'attribute' => 'updateon',
            	//'format' => 'datetime',
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'updateon',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'locale' => [
                            'format' => 'Y-m-d',
                        ],
                    ],
                ]),
                'value' => function ($model) {
            		$time = strtotime($model->updateon);
            		return date('m/d/Y',$time);
				},
            ],
            ['attribute' => 'updatetypeid',
            	'filter'=>ArrayHelper::map(Updatetype::find()->asArray()->all(), 'id', 'type'),
                'value' => function($model) {
                	$updatetype = new Updatetype();
                    return $updatetype->showUpdatetype($model->updatetypeid);
                }
            ],
            ['attribute'=>'oldstatus',
                'filter'=>ArrayHelper::map(Status::find()->asArray()->all(), 'status', 'status'),
                ],
           // 'oldstatus',
           ['attribute'=>'newstatus',
                'filter'=>ArrayHelper::map(Status::find()->asArray()->all(), 'status', 'status'),
                ],
           // 'newstatus',
          //  'oldplan',
            ['attribute' => 'oldplan',
            	'filter'=>ArrayHelper::map(Plans::find()->asArray()->all(), 'planid', 'plan'),
                'value' => function($model) {
                	$plan = new Plans();
                    return $plan->showPlan($model['oldplan']);
                    //return implode(" ",$items);
                }
            ],
            //'newplan',
            ['attribute' => 'newplan',
            	'filter'=>ArrayHelper::map(Plans::find()->asArray()->all(), 'planid', 'plan'),
                'value' => function($model) {
                	$plan = new Plans();
                    return $plan->showPlan($model['newplan']);
                    //return implode(" ",$items);
                }
            ],
            'oldparticulars:ntext',
            'newparticulars:ntext',
            'comments:ntext',
            'username',

           // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
</div>
