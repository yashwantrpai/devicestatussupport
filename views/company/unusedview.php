<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Unused */

$this->title = $model->id;
$this -> params['breadcrumbs'][] = ['label' => $cname, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Unuseds', 'url' => ['unused']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-lg-2">
	     <?= $this->render('/layouts/_menu', [
        'cname' => $cname,
    ]) ?>

	</div>
	<div class="col-lg-10 unused-view">

	    <h1><?= Html::encode($this->title) ?></h1>
	
	    <p>
	        <?= Html::a('Update', ['unusedupdate', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	        <?= Html::a('Delete', ['unuseddelete', 'id' => $model->id], [
	            'class' => 'btn btn-danger',
	            'data' => [
	                'confirm' => 'Are you sure you want to delete this item?',
	                'method' => 'post',
	            ],
	        ]) ?>
	    </p>
	
	    <?= DetailView::widget([
	        'model' => $model,
	        'attributes' => [
	            'id',
	            'imei',
	            'sim',
	            'phone',
	            'devicetypeid',
	            'incdate',
	            'particulars',
	            'status',
	        ],
	    ]) ?>
	</div>
</div>
