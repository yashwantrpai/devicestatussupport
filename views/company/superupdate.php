<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Status;
use app\models\Plans;
use app\models\Devicetype;
//use app\models\Simlogin;

/* @var $this yii\web\View */
/* @var $model app\models\Updates */

$this->title = 'Update Device Status: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => $cname, 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Device Status', 'url' => ['update']];
$this->params['breadcrumbs'][] = ['label' => $model->id];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="row">
    <div class="col-lg-2">
         <?= $this -> render('/layouts/_menu', ['cname' => $cname, ]) ?>

    </div>
    <div class="updates-update col-lg-10">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="updates-form">

    <?php $form = ActiveForm::begin([
        'action' => ['superupdate', 'id'=>$model->id],
        'method' => 'post',
    ]);?>
    
    <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>

    <?= $form->field($model, 'imei')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'sim')->textInput(['maxlength' => true]) ?>
    <?php /*
		$items = ArrayHelper::map(Simlogin::find() -> all(), 'id', 'name');
		echo $form -> field($model, 'simlogin') -> dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Sim Login']);
		*/?>
    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        
    <?php
		$items = ArrayHelper::map(Devicetype::find() -> all(), 'id', 'device');
		echo $form -> field($model, 'devicetypeid') -> dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Device Type']);
		?>
    
    <?php //= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>
    
    <?php
		$items = ArrayHelper::map(Status::find() -> all(), 'status', 'status');
		echo $form -> field($model, 'status') -> dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Status']);
		?>
        
    <?php $items = ArrayHelper::map(Plans::find()->all(), 'planid', 'plan');
    echo $form->field($model, 'planid')->dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Plan']); ?>
    

    <?= $form->field($model, 'particulars')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'incdate')->textInput() ?>

    <?= $form->field($model, 'date')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
</div>
