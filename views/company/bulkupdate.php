<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Status;
use app\models\Plans;
use app\models\Updatetype;
use kartik\date\DatePicker;

$this -> title = 'Device Bulk Update';
$this -> params['breadcrumbs'][] = ['label' => $cname, 'url' => ['index']];
$this -> params['breadcrumbs'][] = $this -> title;
?>

<div class="row">
	<div class="col-lg-2">
		<?= $this -> render('/layouts/_menu', ['cname' => $cname, ]) ?>
	</div>
	<div class="col-lg-6 form-bulkupdate">
		<?php $form = ActiveForm::begin(['action' => ['bulkupdate']]); ?>
		<div class="form-group">
		<?= Html::label('Write each IMEI in a new line: <br />(for new sim and phone use "|" to separate : imei|sim|phone - in said order.)', 'bulkimei', ['class' => 'control-label bulkimei']) ?>
		<?= Html::textarea('bulkimei', '', ['rows' => 5, 'class' => 'form-control']) ?>
		</div>
		<?php
		$items = ArrayHelper::map(Status::find() -> all(), 'id', 'status');
		echo $form -> field($modelUpdate, 'statusid') -> dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Status']);
		?>
		
		<?php
		$items = ArrayHelper::map(Plans::find() -> all(), 'planid', 'plan');
		echo $form -> field($modelUpdate, 'planid') -> dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Plan']);
		?>
		
		<?php
		$items = ArrayHelper::map(Updatetype::find() -> all(), 'id', 'type');
		echo $form -> field($modelUpdate, 'updatetypeid') -> dropDownList($items, ['class' => 'form-control', 'prompt' => 'Select Update Type']);
		?>
		
		 <?= $form->field($modelUpdate, 'updateon')->widget(DatePicker::classname(), [
		    'options' => ['placeholder' => 'Select update date ...'],
		    'pluginOptions' => [
		        'format' => 'mm/dd/yyyy',
		        'todayHighlight' => true
		    ]
		]) ?>
		<?= $form -> field($modelUpdate, 'particulars') -> textarea(['rows' => 3]) ?>

		<div class="form-group">
			<?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
	<div class="col-lg-4 form-submitresult">
		<style>
			.success{color:green; font-size:12px;}
			.error{color:red; font-size:12px;}
		</style>
		<?php
			if(strlen($message)>3){
				echo '<h3>Update Message : </h3>';
				echo $message;
			}
		?>
	</div>
</div>
