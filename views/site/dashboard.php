<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */

$this -> title = 'Device Status :: Dashboard';
?>
<div class="site-index">
    <div class="container">

      <!-- Main component for a primary marketing message or call to action -->
      <div class="panel">
        <h1 class="panel-heading">SpytecGPS :: Device Status</h1>
        <p>To view and modify Device Data of SpytecGPS - </p>
        <p>
            <?= Html::a('Go', ['spytec/index'], ['class' => 'btn btn-lg btn-primary']) ?>
        </p>
      </div>

    </div> <!-- /container -->
</div>