<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */

$this -> title = 'Device Status :: Home';
?>
<div class="site-index">

    <div class="">
        <h1>Welcome to Device Status Application!</h1>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-8">
                <h2>Something goes here</h2>

                <p></p>

            </div>

            <?php if(Yii::$app->user->isGuest){?>
            <div class="col-lg-4">
                <h1><?= Html::encode("Login") ?></h1>

                <p>Please fill out the following fields to login:</p>

                <?php $form = ActiveForm::begin(['action' => ['site/login'],'id' => 'login-form', 'options' => ['class' => 'form-horizontal'], 'fieldConfig' => ['template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\" col-lg-offset-4 col-lg-8\">{error}</div>", 'labelOptions' => ['class' => 'col-lg-4 control-label'], ], ]); ?>

                    <?= $form -> field($model, 'username') -> textInput(['autofocus' => true]) ?>

                    <?= $form -> field($model, 'password') -> passwordInput() ?>


                    <div class="form-group">
                        <div class="col-lg-offset-4 col-lg-8">
                            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                        </div>
                    </div>

                <?php ActiveForm::end(); ?>
            </div>
            <?php }?>
        </div>

    </div>
</div>
