<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Updatetype */

$this->title = 'Create Updatetype';
$this->params['breadcrumbs'][] = ['label' => 'Updatetypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="updatetype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
