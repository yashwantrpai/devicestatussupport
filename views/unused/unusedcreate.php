<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Unused */

$this->title = 'Create Unused';
$this->params['breadcrumbs'][] = ['label' => 'Unuseds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unused-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
