<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Historicaldata */

$this->title = 'Create Historicaldata';
$this->params['breadcrumbs'][] = ['label' => 'Historicaldatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="historicaldata-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
