<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HistoricaldataSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="historicaldata-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'imei') ?>

    <?= $form->field($model, 'comments') ?>

    <?= $form->field($model, 'planid') ?>

    <?php // echo $form->field($model, 'statusid') ?>

    <?php // echo $form->field($model, 'updatetypeid') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
