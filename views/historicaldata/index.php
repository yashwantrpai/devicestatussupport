<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HistoricaldataSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Historicaldatas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="historicaldata-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Historicaldata', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'date',
            'imei',
            'comments',
            'planid',
            // 'statusid',
            // 'updatetypeid',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
