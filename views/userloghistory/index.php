<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserloghistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Login history';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-lg-8 col-lg-offset-2 userloghistory-index">
	
	    <h1><?= Html::encode($this->title) ?></h1>
	    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	
	    
	    <?= GridView::widget([
	        'dataProvider' => $dataProvider,
	        'filterModel' => $searchModel,
	        'pager' => [
			        'firstPageLabel' => 'First',
			        'lastPageLabel' => 'Last',
			    ],
	        'columns' => [
	            ['class' => 'yii\grid\SerialColumn'],
	
	            //'id',
	            'userid',
	            'username',
	            'date',
	            'ip',
	
	            //['class' => 'yii\grid\ActionColumn'],
	        ],
	    ]); ?>
	</div>
</div>