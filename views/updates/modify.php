<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Status;


/* @var $this yii\web\View */
/* @var $model app\models\Updates */

$this->title = 'Update Updates: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Updates', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="updates-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="updates-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'imei')->textInput(['maxlength' => true]) ?>

    <?php $items = ArrayHelper::map(Status::find()->all(), 'id', 'status');
    echo $form->field($model, 'statusid')->dropDownList($items); ?>

    <?= $form->field($model, 'particulars')->textarea(['rows' => 3]) ?>

    <?= $form->field($model, 'datetime')->textInput() ?>

    <?= $form->field($model, 'userid')->textInput() ?>

    <?= $form->field($model, 'ratified')->dropDownList(['0'=>"Pending", '1'=>"Confirmed",]) ?>

    <?= $form->field($model, 'comments')->textarea(['rows' => 3]) ?>

    <div class="form-group">
        <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

</div>
