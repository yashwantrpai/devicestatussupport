<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Plans;

/**
 * PlansSearch represents the model behind the search form about `app\models\Plans`.
 */
class PlansSearch extends Plans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['planid'], 'integer'],
            [['plan', 'plan_tag', 'description'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Plans::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'planid' => $this->planid,
        ]);

        $query->andFilterWhere(['like', 'plan', $this->plan])
            ->andFilterWhere(['like', 'plan_tag', $this->plan_tag])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
