<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property integer $id
 * @property string $imei
 * @property string $content
 * @property string $date
 * @property integer $statusid
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_spytec');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imei', 'date'], 'required'],
            [['content'], 'string'],
            [['date'], 'safe'],
            [['statusid', 'updatetypeid'], 'integer'],
            [['oldsim', 'newsim', 'oldphone', 'newphone'], 'max'=>40],
            [['oldplan', 'newplan'], 'string', 'max' => 50],
            [['imei'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imei' => 'Imei',
            'content' => 'Content',
            'date' => 'Date',
            'updatetypeid'=>'Update Type',
            'statusid' => 'Statusid',
            'oldplan' => 'Old Plan' ,
            'newplan' => 'New Plan',
            'oldsim'=>'Old Sim',
            'newsim'=>'New Sim',
            'oldphone'=>'Old Phone',
            'newphone'=>'New Phone',
        ];
    }
    
    public function lastupdate($imei){
        $res = $this->find()->where(['imei'=>$imei])->orderBy(['date'=>SORT_DESC])->one();
        if($res==null)
            return null;
        
        return $res->updatetypeid;
    }
}
