<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "devicetype".
 *
 * @property integer $id
 * @property string $device
 * @property string $description
 */
class Devicetype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'devicetype';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['device'], 'required'],
            [['description'], 'string'],
            [['device'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'device' => 'Device',
            'description' => 'Description',
        ];
    }
	
	public function showDevice($id){
		
		if($id==null){
			return null;
		}
		$res = $this->find()->where(['id'=>$id])->one();
		if($res!=null){
			return $res->device;
		}else{
			return null;
		}
	}
}
