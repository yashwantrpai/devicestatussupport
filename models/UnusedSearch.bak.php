<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Unused;

/**
 * UnusedSearch represents the model behind the search form about `app\models\Unused`.
 */
class UnusedSearch extends Unused
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'devicetypeid', 'status'], 'integer'],
            [['imei', 'sim', 'phone', 'incdate', 'particulars'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Unused::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'devicetypeid'=> $this->devicetypeid,
            //'incdate' => $this->incdate,
            'particulars' => $this->particulars,
            'status' => $this->status,
        ]);
		
		 $query->andFilterWhere(['like', 'imei', $this->imei])
            ->andFilterWhere(['like', 'sim', $this->sim])
			->andFilterWhere(['like', 'phone', $this->phone]);
		
		if (!is_null($this->incdate) && strpos($this->incdate, ' - ') !== false ) {
	 		$incsplit = explode(" - ", $this->incdate);
            $query->andFilterWhere(['>= DATE', 'STR_TO_DATE(incdate,"%m/%d/%Y")', $incsplit[0]])
				->andFilterWhere(['<= DATE', 'STR_TO_DATE(incdate,"%m/%d/%Y")', $incsplit[1]]);
		}

        return $dataProvider;
    }
}
