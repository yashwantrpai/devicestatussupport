<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "plans".
 *
 * @property integer $planid
 * @property string $plan
 * @property string $plan_tag
 * @property string $description
 */
class Plans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plans';
    }
	
	
	/**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan', 'plan_tag'], 'required'],
            [['description'], 'string'],
            [['plan'], 'string', 'max' => 50],
            [['plan_tag'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'planid' => 'Planid',
            'plan' => 'Plan',
            'plan_tag' => 'Plan Tag',
            'description' => 'Description',
        ];
    }
	
	public function showPlan($planid){
		
		if($planid==null){
			return null;
		}
		$res = $this->find()->where(['planid'=>$planid])->one();
		if($res!=null){
			return $res->plan;
		}else{
			return null;
		}
	}
	
	public function getLikePlanid($param){
		$res = $this->find()->where(['like', 'plan', $param])->one();
		if($res!=null){
			return $res->planid;
		}else{
			return null;
		}
	}
}
