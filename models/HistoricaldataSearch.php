<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Historicaldata;

/**
 * HistoricaldataSearch represents the model behind the search form about `app\models\Historicaldata`.
 */
class HistoricaldataSearch extends Historicaldata
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'planid', 'statusid', 'updatetypeid'], 'integer'],
            [['date', 'imei', 'comments'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Historicaldata::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'planid' => $this->planid,
            'statusid' => $this->statusid,
            'updatetypeid' => $this->updatetypeid,
        ]);

        $query->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'imei', $this->imei])
            ->andFilterWhere(['like', 'comments', $this->comments]);

        return $dataProvider;
    }
}
