<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "simlogin".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 */
class Simlogin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'simlogin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }
	
	public function showProvider($id){
		
		if($id==null){
			return null;
		}
		$res = $this->find()->where(['id'=>$id])->one();
		if($res!=null){
			return $res->name;
		}else{
			return null;
		}
	}
}
