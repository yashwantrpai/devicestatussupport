<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Status;

/**
 * This is the model class for table "unused".
 *
 * @property integer $id
 * @property string $imei
 * @property string $sim
 * @property string $phone
 * @property integer $devicetypeid,
 * @property string $incdate
 * @property string $particulars
 * @property string $status
 */
class Unused extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'unused';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_spytec');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imei'], 'string', 'max' => 30],
            [['sim'], 'string', 'max' => 50],
            [['phone'], 'string', 'max' => 50],
            [['devicetypeid'], 'integer'],
            [['status','incdate'], 'string', 'max' => 50],
            [['particulars'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imei' => 'Imei',
            'sim' => 'Sim',
            'phone' => 'Phone',
            'devicetypeid' => 'Device Type',
            'incdate' => 'Start Date',
            'particulars' => 'Particulars',
            'status' => 'Status',
        ];
    }
	
	public function getStatus($val){
		if($val==null){
			return null;
		}
        $items = ArrayHelper::map(Status::find()->all(), 'id', 'status');
        return $items[$val];
    }
	
	public function findStatusid($val){
		return $this->find('id')->where('status', $val)->one();
	}
}
