<?php

namespace app\models;

use Yii;

class Historicaldata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'historicaldata';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_spytec');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [           
            [['comments'], 'string', 'max' => 200],            
            [['planid','statusid', 'updatetypeid'], 'integer'],           
            [['imei'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',		
			'date' => 'Date',
            'imei' => 'Imei',     
			'comments' => 'Comments',
			'planid' => 'Plan ID',
			'statusid' => 'Status ID',			
            'updatetypeid'=>'Update Type ID'
  
        ];
    }
}
