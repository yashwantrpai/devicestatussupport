<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "updatetype".
 *
 * @property integer $id
 * @property string $type
 * @property string $description
 */
class Updatetype extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'updatetype';
    }
	
	/**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required'],
            [['description'], 'string'],
            [['type'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'description' => 'Description',
        ];
    }
	
	public function showUpdatetype($id){
		
		if($id==null){
			return null;
		}
		$res = $this->find()->where(['id'=>$id])->one();
		if($res!=null){
			return $res->type;
		}else{
			return null;
		}
	}
}
