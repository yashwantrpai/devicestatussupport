<?php

namespace app\models;

use Yii;
use app\models\Status;
use app\models\Plans;
use app\models\Updatetype;
use app\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "history".
 *
 * @property integer $id
 * @property string $imei
 * @property string $date
 * @property string $updateon
 * @property integer $oldstatus
 * @property integer $newstatus
 * @property string $oldparticulars
 * @property string $newparticulars
 * @property string $comments
 * @property string $username
 */
class ChangeHistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_spytec');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
        	[['updatetypeid'], 'integer'],
            [['imei', 'date', 'oldstatus', 'newstatus', 'username'], 'required'],
            [['date'], 'safe'],
            [['updateon', 'oldparticulars', 'newparticulars', 'comments'], 'string'],
            [['imei'], 'string', 'max' => 20],
            [['username','oldstatus', 'newstatus'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imei' => 'Imei',
            'date' => 'Date',
            'updateon' => 'Updated on',
            'updatetypeid'=>'Update Type',
            'oldstatus' => 'Oldstatus',
            'newstatus' => 'Newstatus',
            'oldparticulars' => 'Oldparticulars',
            'newparticulars' => 'Newparticulars',
            'comments' => 'Comments',
            'username' => 'Username',
        ];
    }
	
	public function getStatus($val){
    	if($val==null){
			return null;
		}
        $items = ArrayHelper::map(Status::find()->all(), 'id', 'status');
        return $items[$val];
    }
	
	public function getPlans($val){
		if($val==null){
			return null;
		}
        $items = ArrayHelper::map(Plans::find()->all(), 'planid', 'plan');
        return $items[$val];
    }

	public function getUpdatetype($val){
		if($val==null){
			return null;
		}
        $items = ArrayHelper::map(Updatetype::find()->all(), 'id', 'type');
        return $items[$val];
    }
	
    public function getUser($id){
        $items = ArrayHelper::map(User::find()->all(), 'id', 'username');
        return $items[$id];
    }
}
