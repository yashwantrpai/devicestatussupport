<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "userloghistory".
 *
 * @property integer $id
 * @property integer $userid
 * @property string $username
 * @property string $date
 * @property string $ip
 */
class Userloghistory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'userloghistory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userid'], 'integer'],
            [['date'], 'safe'],
            [['username'], 'string', 'max' => 200],
            [['ip'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userid' => 'Userid',
            'username' => 'Username',
            'date' => 'Date',
            'ip' => 'Ip',
        ];
    }
}
