<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Imeidata;
use app\models\Plans;

/**
 * ImeiSearch represents the model behind the search form about `app\models\Imeidata`.
 */
class ImeiSearch extends Imeidata
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'planid', 'devicetypeid'], 'integer'],
            [['imei', 'sim', 'phone', 'status', 'particulars', 'incdate', 'date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Imeidata::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort'=> ['defaultOrder' => ['date'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,   
            'devicetypeid' => $this->devicetypeid,
            'status' => $this->status,
            
        ]);
		
		
		$incsplit = explode(" - ", $this->incdate);
		
		
        $query->andFilterWhere(['like', 'imei', $this->imei])
            ->andFilterWhere(['like', 'sim', $this->sim])
            ->andFilterWhere(['like', 'phone', $this->phone])
            //->andFilterWhere(['like', 'status', $this->status])
			->andFilterWhere(['like', 'planid', $this->planid])
			//->andFilterWhere(['like', 'devicetypeid', $this->devicetypeid])
            ->andFilterWhere(['like', 'particulars', $this->particulars]);
			//->andFilterWhere(['like', 'incdate', $this->incdate])
			
		if (!is_null($this->incdate) && strpos($this->incdate, ' - ') !== false ) {
	 		$incsplit = explode(" - ", $this->incdate);
            $query->andFilterWhere(['>= DATE', 'STR_TO_DATE(incdate,"%m/%d/%Y")', $incsplit[0]])
				->andFilterWhere(['<= DATE', 'STR_TO_DATE(incdate,"%m/%d/%Y")', $incsplit[1]]);
		}		
			
	 	if (!is_null($this->date) && strpos($this->date, ' - ') !== false ) {
	 		$datesplit = explode(" - ", $this->date);
			$query->andFilterWhere(['>= DATE', 'STR_TO_DATE(date,"%m/%d/%Y")', $datesplit[0]])
				->andFilterWhere(['<= DATE', 'STR_TO_DATE(date,"%m/%d/%Y")', $datesplit[1]]);
			
            //$query->andFilterWhere(['between', 'STR_TO_DATE(dates,"%m/%d/%Y")', ['STR_TO_DATE', "'".$datesplit[0]."'","%m/%d/%Y"], ['STR_TO_DATE', "'".$datesplit[1]."'","%m/%d/%Y"]]);
		}

        return $dataProvider;
    }

	private function dateconvert($string){
		$time = strtotime($string);
        return date('m/d/Y',$time);
	}
}
