<?php

namespace app\models;

use Yii;
use app\models\Plans;

/**
 * This is the model class for table "imeidata".
 *
 * @property integer $id
 * @property string $imei
 * @property string $sim
 * //@property integer $simlogin
 * @property string $phone
 * @property string $status
 * @property integer $planid
 * @property integer $devicetypeid
 * @property string $particulars
 * @property string $date
 * @property string $incdate
 */
class Imeidata extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'imeidata';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_spytec');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imei'], 'string', 'max' => 40],
            [['sim'], 'string', 'max' => 40],
            [['phone'], 'string', 'max' => 40],
            [['planid', 'devicetypeid'], 'integer'],
            [['status','incdate', 'date'], 'string', 'max' => 50],
            [['particulars'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imei' => 'Imei',
            'sim' => 'Sim',
            'phone' => 'Phone',
            'status' => 'Status',
            'planid' => 'Plans',
            'devicetypeid'=>'Device Type',
            'particulars' => 'Comments',
            'incdate' => 'Start Date',
            'date' => 'Last Updated '
        ];
    }
    
}
