<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lastupload".
 *
 * @property integer $uploadid
 * @property string $filename
 * @property string $uploadby
 * @property string $uploaddate
 */
class Lastupload extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lastupload';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_spytec');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filename', 'uploadby', 'uploaddate'], 'required'],
            [['uploaddate'], 'safe'],
            [['filename', 'uploadby'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'uploadid' => 'Uploadid',
            'filename' => 'Filename',
            'uploadby' => 'Uploadby',
            'uploaddate' => 'Uploaddate',
        ];
    }
}
