<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ChangeHistory;

/**
 * HistorySearch represents the model behind the search form about `app\models\ChangeHistory`.
 */
class HistorySearch extends ChangeHistory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'updatetypeid'], 'integer'],
            [['imei', 'date', 'updateon', 'oldsim','newsim', 'oldphone', 'newphone', 'oldstatus', 'newstatus', 'oldplan', 'newplan', 'oldparticulars', 'newparticulars', 'comments', 'username'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChangeHistory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['date'=>SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'updatetypeid'=>$this->updatetypeid
           // 'date' => $this->date,
        ]);
        $query->andFilterWhere(['like', 'imei', $this->imei])
		//->andFilterWhere(['like', 'updateon', $this->updateon])
            ->andFilterWhere(['like', 'oldsim', $this->oldsim])
            ->andFilterWhere(['like', 'newsim', $this->newsim])
            ->andFilterWhere(['like', 'oldphone', $this->oldphone])
            ->andFilterWhere(['like', 'newphone', $this->newphone])
            
            ->andFilterWhere(['like', 'oldstatus', $this->oldstatus])
            ->andFilterWhere(['like', 'newstatus', $this->newstatus])
            ->andFilterWhere(['like', 'oldparticulars', $this->oldparticulars])
            ->andFilterWhere(['like', 'newparticulars', $this->newparticulars])
            ->andFilterWhere(['like', 'comments', $this->comments])
            ->andFilterWhere(['like', 'username', $this->username]);
			
		if (!is_null($this->date) && strpos($this->date, ' - ') !== false ) {
	 		$incsplit = explode(" - ", $this->date);
            $query->andFilterWhere(['>= DATE', 'date', $incsplit[0]])
				->andFilterWhere(['<= DATE', 'date', $incsplit[1]]);
		}
		
		if (!is_null($this->updateon) && strpos($this->updateon, ' - ') !== false ) {
	 		$incsplit = explode(" - ", $this->updateon);
            $query->andFilterWhere(['>= DATE', 'STR_TO_DATE(updateon,"%m/%d/%Y")', $incsplit[0]])
				->andFilterWhere(['<= DATE', 'STR_TO_DATE(updateon,"%m/%d/%Y")', $incsplit[1]]);
		}
		
        return $dataProvider;
    }
}
