<?php

namespace app\models;

use Yii;

use app\models\Status;
use app\models\Plans;
use app\models\Updatetype;
use app\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "updates".
 *
 * @property integer $id
 * @property string $imei
 * @property string $sim
 * @property string $phone
 * @property integer $statusid
 * @property integer $planid
 * @property integer $updatetypeid
 * @property string $particulars
 * @property string $updateon
 * @property string $datetime
 * @property integer $userid
 * @property integer $ratified
 * @property string $comments
 */
class Updates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'updates';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('db_spytec');
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['imei', 'userid'], 'required'],
            [['statusid', 'planid', 'updatetypeid', 'userid', 'ratified'], 'integer'],
            [['sim', 'phone','particulars', 'updateon', 'comments'], 'string'],
            [['datetime'], 'safe'],
            [['imei'], 'string', 'max' => 20],
            [['sim', 'phone'], 'string', 'max' => 40]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'imei' => 'Imei',
            'sim'=>'Sim',
            'phone'=>'Phone',
            'statusid' => 'Status',
            'planid' => 'Plan',
            'updatetypeid' => 'Update Type',
            'particulars' => 'Comments',
            'updateon' => 'Updated on',
            'datetime' => 'Datetime',
            'userid' => 'Userid',
            'ratified' => 'Ratified',
            'comments' => 'Comments',
        ];
    }

    public function getStatus($val){
    	if($val==null){
			return null;
		}
        $items = ArrayHelper::map(Status::find()->all(), 'id', 'status');
        return $items[$val];
    }
	
	public function getPlans($val){
		if($val==null){
			return null;
		}
        $items = ArrayHelper::map(Plans::find()->all(), 'planid', 'plan');
        return $items[$val];
    }

	public function getUpdatetype($val){
		if($val==null){
			return null;
		}
        $items = ArrayHelper::map(Updatetype::find()->all(), 'id', 'type');
        return $items[$val];
    }

    public function getUser($id){
        $items = ArrayHelper::map(User::find()->all(), 'id', 'username');
        return $items[$id];
    }
}
