<?php

namespace app\controllers;

use Yii;
use app\models\Simlogin;
use app\models\SimloginSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;

use yii\filters\AccessControl;

use app\models\Imeidata;
use app\models\ODSread;
use app\models\ODSwrite;

/**
 * SimloginController implements the CRUD actions for Simlogin model.
 */
class SimloginController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
	            'class' => AccessControl::className(),
	            'rules' => [
	                [
	                    'allow' => true,
	                    'roles' => ['@'],
	                ],
	            ],
	    	],
        ];
    }

    /**
     * Lists all Simlogin models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SimloginSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Simlogin model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Simlogin model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Simlogin();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Simlogin model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Simlogin model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Simlogin model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Simlogin the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Simlogin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionUpload()
    {
        ini_set('max_execution_time', 900);
        $model = new \app\models\Uploadfile();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->upload()) {
                $path = ODSread::extract_content_xml('../web/upload/'.$model->imageFile->name,"./temp");
				$reader = new ODSread(0,"".$path);
				foreach ($reader->rows_data as $key => $value) {
					$simloginid=0;	                
					if(($modelsimlogin = Simlogin::find()->where(['name'=>$value['B']])->one())==null){
						$modelsimlogin = new Simlogin();
						$modelsimlogin->name = $value['B'];
						$modelsimlogin->description = $value['B'];
						$modelsimlogin->save(false);
						
						$simloginid = $modelsimlogin->getPrimaryKey();
					}else{
						$simloginid = $modelsimlogin->id;
					};	
					
					if(($modelimei = Imeidata::find()->where(['sim'=>trim($value['A'])])->one())!==null){
						$modelimei->simlogin = $simloginid;
						$modelimei->save(false);
					}else{
						echo "<br />not found = ".$value['A'];
					}
				}						
            }
        }else{
        	return $this->render('upload', ['filedata'=>$model]);
        }
    }
}
