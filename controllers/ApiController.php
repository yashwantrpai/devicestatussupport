<?php
    
namespace app\controllers;

use Yii;
use yii\filters\Cors;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\ArrayHelper;


use app\models\Imeidata;
use app\models\Simlogin;


class ApiController extends Controller
{

    public function behaviors()
    {
    return ArrayHelper::merge([
        [
            'class' => Cors::className(),
            'cors' => [
                'Origin' => ["http://www.spytecgps2.com"],
                'Access-Control-Request-Method' => ['GET', 'HEAD', 'OPTIONS'],
            ],
        ],
    ], parent::behaviors());
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex($imei)
    {
        Yii::$app -> response -> format = Response::FORMAT_JSON;
        $userIP = Yii::$app->getRequest()->getUserIP();
       
       
       /*    ***********  to add your ip - please use another or "||"    *************   */
       /*    ***********  to add your ip - please use another or "||"    *************   */
       /*    ***********  to add your ip - please use another or "||"    *************   */
       if($userIP == "45.113.101.208" || $userIP == "162.243.30.225"  || $userIP == "72.14.191.151" || $userIP == "96.126.111.46"){
           //$model = new Imeidata();
           $model = Imeidata::find()->where(['imei'=>$imei])->asArray()->one();
		   $provider = new Simlogin();
		   $model['simlogin'] = $provider->showProvider($model['simlogin']);
           return $model;
       }else{
           return "You do not have access to this data. ";
       }       
    }
    
    protected function findModel($id)
    {
        if (($model = Imeidata::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}