<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;

use yii\web\UploadedFile;
use yii\web\Response;
use yii\db\Query;
use yii\data\SqlDataProvider;

use app\models\Imeidata;
use app\models\ImeiSearch;
use app\models\Status;
use app\models\Plans;
use app\models\Updates;
//use app\models\Simlogin;
use app\models\UpdatesSearch;
use app\models\ODSread;
use app\models\ChangeHistory;
use app\models\History;
use app\models\HistorySearch;
use app\models\Historicaldata;
use app\models\Lastupload;

use app\models\Unused;
use app\models\UnusedSearch;

use app\models\ODSwrite;

use PHPExcel;
use PHPExcel_IOFactory;
use PHPExcel_Cell_DataType;


class CompanyController extends Controller
{
    public $jsFile = '';

    public $companyname="";

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'Deleteupdate' => ['post'],
                ],
            ],
            'access' => [
	            'class' => AccessControl::className(),
	            'rules' => [
	                [
	                    'allow' => true,
	                    'roles' => ['@'],
	                ],
	            ],
	    	],
        ];
    }

    public function actionCreate()
    {
        $model = new Imeidata();
		$model1 = new Historicaldata();

        if ($model->load(Yii::$app->request->post()) ) {

            $items = ArrayHelper::map(Status::find()->all(), 'id', 'status');
			$statusid = $model->status;
            $model->status = $items[$model->status];
			$model->particulars="" ;
            $model->save();

			$model1->imei = $model->imei;
			$model1->date = $model->date;
			$model1->comments = $model->particulars;
			$model1->planid = $model->planid;
			$model1->statusid = $statusid;
			$model1->updatetypeid = 1;
            $model1->save();

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    public function actionIndex()
    {
    	$this -> jsFile = '@app/views/company/company.js';

        // Publish and register the required JS file
        Yii::$app -> assetManager -> publish($this -> jsFile);
        $this -> getView() -> registerJsFile(Yii::$app -> assetManager -> getPublishedUrl($this -> jsFile), ['yii\web\YiiAsset'] // depends
        );



        ini_set('max_execution_time', 600);
        $searchModel = new ImeiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = ['pageSize' => 100];

		$lastupload = Lastupload::find()->orderBy(['uploadid'=> SORT_DESC])->one();

        return $this->render('/company/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cname'=>$this->companyname,
            'lastupload'=>$lastupload,
        ]);

    }

   /* public function actionInit()
    {
        return $this->render('init');
    }*/
    public function actionUpdate($id = null)
    {
    	if(isset($id) && Yii::$app->user->identity->username=='sapna'){
			$model = Imeidata::find()->where(['id'=>$id])->one();
			 return $this->render('/company/superupdate', [
                'model' => $model,
                'cname'=>$this->companyname,
            ]);
    	}

        $modelUpdate = new Updates();

        if ($modelUpdate->load(Yii::$app->request->post())){
            //$modelUpdate->datetime = //timestamp
            $modelUpdate->userid = Yii::$app->user->getId();
            $modelUpdate->ratified = 0;
            $modelUpdate->comments = "";
			if($modelUpdate->updateon==null){
				$modelUpdate->updateon = date("m/d/Y");
			}
            $modelUpdate->save(false);
        }

        $this -> jsFile = '@app/views/company/company.js';

        // Publish and register the required JS file
        Yii::$app -> assetManager -> publish($this -> jsFile);
        $this -> getView() -> registerJsFile(Yii::$app -> assetManager -> getPublishedUrl($this -> jsFile), ['yii\web\YiiAsset'] // depends
        );

        $searchModel = new ImeiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		$dataProvider->pagination = ['pageSize' => 100];

		// $count = Yii::$app->db->createCommand('SELECT COUNT(*) FROM devicestatus_spytec.imeidata')->queryScalar();
		// echo $count;

		//$provider = new SqlDataProvider([
			//'sql' => 'SELECT * FROM devicestatus_spytec.imeidata',
			//'totalCount' => $count,
			//'pagination' => [
				//'pageSize' => 100,
			//],
		//]);
		//$dataProvider = $provider;
		//print_r($dataProvider);

		//$lastupload = Lastupload::find()->orderBy(['uploadid'=> SORT_DESC])->one();

        return $this->render('/company/update', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cname'=>$this->companyname,
            //'lastupload'=>$lastupload,
        ]);

        //return $this->render('update');
    }

	public function actionSuperupdate($id){
		if(Yii::$app->user->identity->username!='sapna'){
			print_r("Unauthorized Access ..... ");
		}
		$modelUpdate = Imeidata::find()->where(['id'=>$id])->one();

		if ($modelUpdate->load(Yii::$app->request->post())){
            $modelUpdate->save(false);
        }

		return $this->redirect(['update']);
	}

	public function actionBulkupdate(){
		$modelUpdate = new Updates();
		$updatesMsg = '';
		if (Yii::$app->request->isPost) {
			$imeis = explode(PHP_EOL, Yii::$app->request->post('bulkimei'));
			$updates = Yii::$app->request->post('Updates');

			foreach ($imeis as $key => $value) {
				if(strlen($value)<5){
					continue;
				}
                                $inclsimphone = explode("|", $value);

				$imeiModel = Imeidata::find()->where(['imei'=>trim($inclsimphone[0])])->one();
                if($imeiModel !=null){
                	$modelUpdate = new Updates();
					$modelUpdate->imei = trim($inclsimphone[0]);

                                        if(count($inclsimphone)>1){
                                                $modelUpdate->sim = trim($inclsimphone[1]);
                                        }else{
                                            $modelUpdate->sim = $imeiModel->sim;
                                        }
                                        if(count($inclsimphone)>2){
                                                $modelUpdate->phone = trim($inclsimphone[2]);
                                        }else{
                                            $modelUpdate->phone = $imeiModel->phone;
                                        }

                    $template=$this->getTemplateForParticular($updates['planid'],$updates['updatetypeid'],$updates['updateon'],$updates['particulars']);
					$modelUpdate->statusid = $updates['statusid'];
					$modelUpdate->planid = $updates['planid'];
					$modelUpdate->updatetypeid = $updates['updatetypeid'];
					$modelUpdate->particulars =$template;
					$modelUpdate->updateon = $updates['updateon'];
					$modelUpdate->userid = Yii::$app->user->getId();
            		$modelUpdate->ratified = 0;
            		$modelUpdate->comments = $updates['particulars'];
					$modelUpdate->save(false);

                	$updatesMsg .='<p class="success">IMEI :  '.$value.' Added to updates.</p>';
				}else{
					$updatesMsg .='<p class="error">IMEI :  '.$value.' Not found with existing devices.<br />Check if New.</p>';
				}
			}

		}
		return $this->render('/company/bulkupdate', [
            'cname'=>$this->companyname,
            'modelUpdate'=>$modelUpdate,
            'message'=>$updatesMsg,
        ]);

	}
    public function getTemplateForParticular($planid,$updatetypeid,$date,$comment){
		  $tempmodel = new ChangeHistory();
		  $updatetype=$tempmodel->getUpdatetype($updatetypeid);
		  $plan=$tempmodel->getPlans($planid);
		  if($updatetypeid==1){
			  $template=$comment." on ".$date;
		  }
		  else if ($updatetypeid==3){
			   $template="Upgrade to ".$plan." on ".$date;
		  }
		  else if ($updatetypeid==2){
			   $template="Downgrade to ".$plan." on ".$date;
		  }
		  else if ($updatetypeid==4){
			   $template="Killed on ".$date;
		  }
		  else if ($updatetypeid==5){
			   $template="Reactivated on ".$date;
		  }
		  else if ($updatetypeid==6){
			   $template="Renewed on ".$date;
		  }
		  return $template;
	 }
    public function actionLastupdate($id,$imei){
        if (Yii::$app -> request -> isAjax) {
            Yii::$app -> response -> format = Response::FORMAT_JSON;

            $modelUpdate = Imeidata::find()->where(['imei'=>$imei])->one();

			if($modelUpdate!=null){
				$status = new Status();
				$id = $status->getLikeStatus($modelUpdate->status);
				$res = array('imeidata'=>$modelUpdate, 'status_id'=>$id);
			}

            return $modelUpdate!=null?$res:"False";
        }
    }

    public function actionUpload()
    {
        ini_set('max_execution_time', 300);
        $model = new Imeidata();
         $model = new \app\models\Uploadfile();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
            if ($model->upload()) {

                $path = ODSread::extract_content_xml('../web/upload/'.$model->imageFile->name,"./temp");

                return $this->render('/company/upload',['cname'=>$this->companyname, 'filedata'=>$model, 'msg'=>true, 'path'=>$path]);
            }
        }

        //return $this->render('upload', ['model' => $model]);
        return $this->render('/company/upload',['cname'=>$this->companyname,'filedata'=>$model,'msg'=>FALSE]);

    }

	public function actionUploadunused(){
		 ini_set('max_execution_time', 300);
        $model = new Imeidata();
         $model = new \app\models\Uploadfile();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');
			$uploadmodel = new Lastupload();
			$uploadmodel->filename = $model->imageFile->name;
			$uploadmodel->uploadby = Yii::$app->user->identity->username;
			$uploadmodel->uploaddate = date("Y-m-d");
			$uploadmodel->save();

            if ($model->upload()) {

                $path = ODSread::extract_content_xml('../web/upload/'.$model->imageFile->name,"./temp");

                return $this->render('/company/uploadunused',['cname'=>$this->companyname, 'filedata'=>$model, 'msg'=>true, 'path'=>$path]);
            }
        }

        //return $this->render('upload', ['model' => $model]);
        return $this->render('/company/uploadunused',['cname'=>$this->companyname,'filedata'=>$model,'msg'=>FALSE]);
	}

	public function actionRenderunused($path=null){
        ini_set('max_execution_time', 900);
        ini_set('memory_limit', '512M');

        $connection = Unused::getDb();
        $command = $connection->createCommand('TRUNCATE devicestatus_'.$this->companyname.'.unused;');
        $command->execute();

        $reader = new ODSread(0,"".$path);

       // $flag = false;

        foreach ($reader->rows_data as $key => $value) {
          //  if(!$flag){   // this is to jump the first row containing the column heads.
          //      $flag = TRUE;
          //  }else{
                $model = new Unused();
                //$model->id = $reader->rows_data;
                $model->imei = isset($value['A'])?$value['A']:" ";
                $model->sim = isset($value['B'])?$value['B']:" ";
                $model->phone = isset($value['C'])?$value['C']:" ";
                $model->incdate = isset($value['D'])?$value['D']:"";
				$model->particulars = isset($value['E'])?$value['E']:"";
				$model->status = $this->findStatus(isset($value['F'])?$value['F']:"");
				$model->devicetypeid = isset($value['G'])?$value['G']:null;

                try{
                    $model->save(false);
                }catch(Exception $e){
                    echo "Error ! ".$e->message;
                    die;
                }
           // }

        }
		 return $this->redirect(['/'.$this->companyname.'/unused', 'cname'=>$this->companyname,]);

       /* $searchModel = new UnusedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/company/unused', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cname'=>$this->companyname,
        ]);*/
    }





    public function actionRender($path=null){
        ini_set('max_execution_time', 900);
        ini_set('memory_limit', '512M');

        $connection = Imeidata::getDb();
		$connection1 = Historicaldata::getDb();
        $command = $connection->createCommand('TRUNCATE devicestatus_'.$this->companyname.'.imeidata;');
        $command->execute();
		$command1 = $connection1->createCommand('TRUNCATE devicestatus_'.$this->companyname.'.historicaldata;');
        $command1->execute();
        $reader = new ODSread(0,"".$path);
       // $flag = false;

        foreach ($reader->rows_data as $key => $value) {
          //  if(!$flag){   // this is to jump the first row containing the column heads.
          //      $flag = TRUE;
          //  }else{
                $model = new Imeidata();
                //$model->id = $reader->rows_data;
                $model->imei = isset($value['A'])?$value['A']:" ";
                $model->sim = isset($value['B'])?$value['B']:" ";
                $model->phone = isset($value['C'])?$value['C']:" ";
                $model->status = isset($value['D'])?$value['D']:"";
                $model->particulars = isset($value['F'])?$value['F']:"";
                $model->incdate = isset($value['G'])?$value['G']:"";
				$model->date = isset($value['H'])?$value['H']:"";
				$model->planid = $this->findPlan(isset($value['E'])?$value['E']:"");
				$model->devicetypeid = isset($value['I'])?$value['I']:"";
				//$model->simlogin = isset($value['J'])?$value['J']:"";

				$model1 = new Historicaldata();
                //$model->id = $reader->rows_data;
                $model1->imei = isset($value['A'])?$value['A']:" ";
                $model1->comments = isset($value['F'])?$value['F']:"";
                $model1->date = isset($value['H'])?$value['H']:"";
				$model1->planid = isset($value['E'])?$value['E']:"";
                try{
                    $model->save();
					$model1->save();
                }catch(Exception $e){
                    echo "Error in save ! ".$e->message;
                    die;
                }
           // }

        }


		 return $this->redirect(['/'.$this->companyname.'/index', 'cname'=>$this->companyname,]);
		/*
        $searchModel = new ImeiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/company/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cname'=>$this->companyname,
        ]);*/
    }


    public function actionAppend(){
        ini_set('max_execution_time', 300);
        $model = new Imeidata();
         $model = new \app\models\Uploadfile();

        if (Yii::$app->request->isPost) {
            $model->imageFile = UploadedFile::getInstance($model, 'imageFile');

			$uploadmodel = new Lastupload();
			$uploadmodel->filename = $model->imageFile->name;
			$uploadmodel->uploadby = Yii::$app->user->identity->username;
			$uploadmodel->uploaddate = date("Y-m-d");
			$uploadmodel->save();

            if ($model->upload()) {

                $path = ODSread::extract_content_xml('../web/upload/'.$model->imageFile->name,"./temp");

                return $this->render('/company/append',['cname'=>$this->companyname, 'filedata'=>$model, 'msg'=>true, 'path'=>$path]);
            }
        }

        //return $this->render('upload', ['model' => $model]);
        return $this->render('/company/append',['cname'=>$this->companyname,'filedata'=>$model,'msg'=>FALSE]);
    }

    public function actionRenderappend($path=null){
        ini_set('max_execution_time', 900);
        ini_set('memory_limit', '512M');

        $reader = new ODSread(0,"".$path);

       // $flag = false;
       $duplicates = array();

        foreach ($reader->rows_data as $key => $value) {
           /* if(isset($value['A'])&& Imeidata::find()->where(['imei'=>trim($value['A'])])->one()==null){
                array_push($duplicates, $value['A']);
                continue;
            }*/
          //  if(!$flag){   // this is to jump the first row containing the column heads.
          //      $flag = TRUE;
          //  }else{
                $model = new Imeidata();
                //$model->id = $reader->rows_data;
                $model->imei = isset($value['A'])?$value['A']:" ";
                $model->sim = isset($value['B'])?$value['B']:" ";
                $model->phone = isset($value['C'])?$value['C']:" ";
                $model->status = isset($value['D'])?$value['D']:"";
                $model->particulars = isset($value['E'])?$value['E']:"";
				$model->incdate = isset($value['F'])?$value['F']:"";
				$model->date = isset($value['F'])?$value['F']:"";
				$model->planid = $this->findPlan(isset($value['G'])?$value['G']:"");
				$model->devicetypeid = isset($value['H'])?$value['H']:"";
				//$model->simlogin = isset($value['I'])?$value['I']:"";
                try{
                    $model->save();
                }catch(Exception $e){
                    echo "Error ! ".$e->message;
                    die;
                }
           // }
        }

		return $this->redirect(['/'.$this->companyname.'/index', 'cname'=>$this->companyname, 'duplicates'=>$duplicates]);
		/*
        $searchModel = new ImeiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/company/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cname'=>$this->companyname,
        ]);
		 */
    }

	public function actionUpdatelist()
    {
			$this -> jsFile = '@app/views/company/company.js';

			// Publish and register the required JS file
			Yii::$app -> assetManager -> publish($this -> jsFile);
			$this -> getView() -> registerJsFile(Yii::$app -> assetManager -> getPublishedUrl($this -> jsFile), ['yii\web\YiiAsset'] // depends
			);

			$searchModel = new UpdatesSearch();
			$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
			$dataProvider->pagination = ['pageSize' => 100];

			return $this->render('/company/updatelist', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'cname'=>$this->companyname,
			]);
    }

	public function actionDuplicateupdates()
    {
		Yii::$app -> response -> format = Response::FORMAT_JSON;
		$ret = Updates::find()
				->select(['imei', 'updatetypeid', 'COUNT(*) as cnt'])
				->where('ratified = 0')
				->groupBy('imei')
				->having('cnt > 1')
				->all();
		return $ret;
    }

    public function actionDeleteupdate($id)
    {
        $this->doDeleteUpdate($id);
        return $this->redirect(['/'.$this->companyname.'/updatelist']);
    }

    private function doDeleteUpdate($id){
        $model = Updates::find()->where(['id'=>$id])->one()->delete();
    }

    public function actionHistory($id,$imei)
    {

        if (Yii::$app -> request -> isAjax) {
            Yii::$app -> response -> format = Response::FORMAT_JSON;

            $modelHistory = ChangeHistory::find()->where(['imei'=>$imei])->orderBy(['date'=>SORT_DESC])->all();
            foreach ($modelHistory as $key => $value) {
                $tempmodel = new ChangeHistory();
				$value->newplan = $tempmodel->getPlans($value->newplan);
				$value->updatetypeid = $tempmodel->getUpdatetype($value->updatetypeid);
            }
            return $modelHistory!=null?$modelHistory:"False";
        }
    }

    public function actionUpdatehistory()
    {
        $searchModel = new HistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = ['pageSize' => 100];
        return $this->render('/company/changehistory', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cname'=>$this->companyname,
        ]);
    }

	public function actionGetquerieddevice()
	{
		$model = new ChangeHistory();
		$date = "";
		$request = Yii::$app->request;
		if($model->load($request->post())) {
			$date = $model['date'];	
			$date = "%".$date."%";
			$count = Yii::$app->db->createCommand('SELECT oldstatus, newstatus FROM devicestatus_spytec.history WHERE date LIKE :date', [':date' => $date])->queryScalar();

			$dataProvider = new SqlDataProvider([
				'sql' => 'SELECT oldstatus, newstatus FROM devicestatus_spytec.history WHERE date LIKE :date',
				'params' => [':date' => $date],
				'totalCount' => $count,
				'pagination' => [
					'pageSize' => 20,
				],
			]);
		
			$date = str_replace("%", "", $date);	
			return $this->render('/company/getquerieddevice', [
              'model' => $model,
			  'dataProvider' => $dataProvider,
			  'date' => $date,
			]);	
		} else {
			
		}
	}	

	public function actionGetstatusdevice(){
		$model = new ChangeHistory();
		return $this->render('/company/getstatusdevice', [
               'model' => $model,
        ]);
	}

    public function actionConfirm($id)
    {
        $this->doConfirm($id);
        return $this->redirect(['/'.$this->companyname.'/updatelist', 'cname'=>$this->companyname,]);
    }

    private function doConfirm($id){

        $model = $this->findModel($id);
		$imei = $model->imei;
		
        $model->ratified = true;
        if($model->save()){        	
            $imeiModel = Imeidata::find()->where(['imei'=>trim($imei)])->one();
	        if($imeiModel !=null){
	            $historyModel = new ChangeHistory();
				$historicaldataModel= new Historicaldata();
                $historyModel->imei = $model->imei;
	            $historyModel->oldsim = $imeiModel->sim;
                $historyModel->newsim = $model->sim==""?$imeiModel->sim:$model->sim;
                $historyModel->oldphone = $imeiModel->phone;
                $historyModel->newphone = $model->phone==""?$imeiModel->phone:$model->phone;                   
                    
	            $historyModel->newstatus = ($model->getStatus($model->statusid)==null)?$imeiModel->status:$model->getStatus($model->statusid);
				$historyModel->newplan = ($model->planid=="")?$imeiModel->planid:$model->planid;
				$historyModel->updatetypeid= $model->updatetypeid;
	            $historyModel->newparticulars = $model->particulars;
	            $historyModel->comments= $model->comments;
	            $historyModel->oldstatus = $imeiModel->status;
				$historyModel->oldplan = $imeiModel->planid;
	            $historyModel->oldparticulars = $imeiModel->particulars;
	            $historyModel->username = Yii::$app->user->identity->username;
	            $historyModel->date = $model->datetime;
				$historyModel->updateon = $model->updateon;
	            $historyModel->save();
	           
	           $imeiModel->status = ($model->getStatus($model->statusid)==null)?$imeiModel->status:$model->getStatus($model->statusid);
			   $imeiModel->planid = ($model->planid=="")?$imeiModel->planid:$model->planid;
	           $imeiModel->particulars=$imeiModel->particulars.'; '.$model->particulars;
	           $imeiModel->date = ($model->updateon==null)?$this->dateconvert($model->datetime):$model->updateon;
			   
			   $historicaldataModel->statusid= $model->statusid;
               $historicaldataModel->planid = $model->planid;
			   $historicaldataModel->updatetypeid = $model->updatetypeid;
			   $historicaldataModel->imei = $model->imei; 
			   $historicaldataModel->comments= $model->comments;
			   $historicaldataModel->date = $model->updateon; 
			   $historicaldataModel->save(false);
  			   
                   if($imeiModel->sim != $model->sim){
                         $imeiModel->sim = $model->sim;
                   }
                   
                   if($imeiModel->phone != $model->phone){
                         $imeiModel->phone = $model->phone;
                   }
	           $imeiModel->save(); 
	        }
		
		}
    }
	private function dateconvert($string){
		$time = strtotime($string);
        return date('m/d/Y',$time);
	}

     public function actionModify($id){
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/'.$this->companyname.'/updatelist', 'cname'=>$this->companyname,]);
        } else {
            return $this->render('/company/modify', [
                'model' => $model,
                'cname'=>$this->companyname,
            ]);
        }
     }

     public function actionBulk(){
        $action=Yii::$app->request->post('action');
        $selection=(array)Yii::$app->request->post('selection');
        if($action == '20'){//confirmed
            foreach($selection as $id){
                $this->doConfirm($id);
            }
        }elseif($action == '30'){  //delete
            foreach($selection as $id){
                $this->doDeleteUpdate($id);
            }
        }else{
            // do nothing
        }
         return $this->redirect(['/'.$this->companyname.'/updatelist', 'cname'=>$this->companyname,]);
    }

    public function actionView($id)
    {
        return $this->render('/company/view', [
            'model' => $this->findModel($id),
            'cname'=>$this->companyname,
        ]);
    }
    protected function findModel($id)
    {
        if (($model = Updates::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	private function findPlan($plantag){
		if($plantag == ''){
			return null;
		}else{
			$res = Plans::find()->where(['plan_tag'=>trim($plantag)])->one();
			if($res !=null){
				return $res->planid;	
			}else{
				return null;
			}
		}
	}
	
	private function findStatus($status){
		if($status == ''){
			return null;
		}else{
			$res = Status::find()->where(['status'=>trim($status)])->one();
			if($res !=null){
				return $res->id;	
			}else{
				return null;
			}
		}
	}
	
	
	/*    *****  For unused devices  *****     */
	
	/**
     * Lists all Unused models.
     * @return mixed
     */
    public function actionUnused()
    {
        $searchModel = new UnusedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$dataProvider->pagination = ['pageSize' => 100];
        return $this->render('/company/unused', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'cname'=>$this->companyname,
        ]);
    }

    /**
     * Displays a single Unused model.
     * @param integer $id
     * @return mixed
     */
    public function actionUnusedview($id)
    {
        return $this->render('/company/unusedview', [
            'model' => $this->findUnusedModel($id),
            'cname'=>$this->companyname,
        ]);
    }

    /**
     * Creates a new Unused model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUnusedcreate()
    {
        $model = new Unused();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/'.$this->companyname.'/unusedview', 'id' => $model->id, 'cname'=>$this->companyname,]);
        } else {
            return $this->render('/company/unusedcreate', [
                'model' => $model,
                'cname'=>$this->companyname,
            ]);
        }
    }

    /**
     * Updates an existing Unused model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUnusedupdate($id)
    {
        $model = $this->findUnusedModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/'.$this->companyname.'/unusedview', 'id' => $model->id, 'cname'=>$this->companyname,]);
        } else {
            return $this->render('/company/unusedupdate', [
                'model' => $model,
                'cname'=>$this->companyname,
            ]);
        }
    }

    /**
     * Deletes an existing Unused model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUnuseddelete($id)
    {
        $this->findUnusedModel($id)->delete();

        return $this->redirect(['/'.$this->companyname.'/unused']);
    }

    /**
     * Finds the Unused model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Unused the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findUnusedModel($id)
    {
        if (($model = Unused::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	
	public function actionExportall(){
	    ini_set('max_execution_time', 900);
		ini_set('memory_limit', '-1');
		$imeidata = Imeidata::find()->asArray()->all();
       
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Bhaskar Bhattachariya")
							 ->setLastModifiedBy("Bhaskar Bhattachariya")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("php data export result file");
		
		$cellarray = explode(',', "A,B,C,D,E,F,G,H,I,J,K,L,K,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z");  //please increase the number here if you need
		$counter = 1; // since 1 based		
		$objPHPExcel->setActiveSheetIndex(0);		
		foreach ($imeidata as $key => $value) {
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit("A".$counter, $value['imei'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("B".$counter, $value['sim'], PHPExcel_Cell_DataType::TYPE_STRING)
                //->setCellValueExplicit("C".$counter, $value['simlogin'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("D".$counter, $value['phone'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("E".$counter, $value['status'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("F".$counter, $value['planid'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("G".$counter, $value['particulars'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("H".$counter, $value['incdate'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("I".$counter, $value['date'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("J".$counter, $value['devicetypeid'], PHPExcel_Cell_DataType::TYPE_STRING);                

			$counter++;
		}
		
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Totals');
		
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);		
		
		// Redirect output to a client’s web browser (OpenDocument)
		header('Content-Type: application/vnd.oasis.opendocument.spreadsheet');
		header('Content-Disposition: attachment;filename="imei1.ods"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		//header('Cache-Control: max-age=1');
		
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 2016 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'OpenDocument');
		$objWriter->save('php://output');
	}
	
		public function actionExportdate(){
		$exportdata = array();
		$titles = array ('imei' => 'IMEI', 'sim' => 'SIM', 'phone' => 'PHONE', 'devicetypeid' => 'DEVICE TYPE ID', 'status' => 'STATUS', 'planid' => 'PLAN ID', 'particulars' => 'PARTICULARS', 'date' => 'DATE', 'incdate' => 'INCDATE'); 
		array_push($exportdata, $titles);
		$imeidata = (new \yii\db\Query())
						->select(['imei', 'sim', 'phone', 'devicetypeid', 'status', 'planid', 'particulars', 'date', 'incdate'])
						->from('devicestatus_spytec.imeidata')
						->where(['>', 'date', '2017-09-01'])
						->all();
						
		for($i = 0; $i < count($imeidata); $i++){
			array_push($exportdata, $imeidata[$i]);	
		}			
	
		ini_set('max_execution_time', 900);
		ini_set('memory_limit', '-1');
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Bhaskar Bhattachariya")
							 ->setLastModifiedBy("Bhaskar Bhattachariya")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("php data export result file");
		
		$cellarray = explode(',', "A,B,C,D,E,F,G,H,I,J,K,L,K,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z");  //please increase the number here if you need
		$counter = 1; // since 1 based		
		$objPHPExcel->setActiveSheetIndex(0);		
		foreach ($exportdata as $key => $value) {
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit("A".$counter, $value['imei'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("B".$counter, $value['sim'], PHPExcel_Cell_DataType::TYPE_STRING)
				//->setCellValueExplicit("C".$counter, $value['simlogin'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("D".$counter, $value['phone'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("E".$counter, $value['devicetypeid'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("F".$counter, $value['status'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("G".$counter, $value['planid'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("H".$counter, $value['particulars'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("I".$counter, $value['date'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("J".$counter, $value['incdate'], PHPExcel_Cell_DataType::TYPE_STRING);

			$counter++;
		}
		
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Update list');
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);		
		
		// Redirect output to a client’s web browser (OpenDocument)
		header('Content-Type: application/vnd.oasis.opendocument.spreadsheet');
		header('Content-Disposition: attachment;filename="imei1.ods"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		//header('Cache-Control: max-age=1');
		
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 2016 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'OpenDocument');
		$objWriter->save('php://output');
	}
	
	public function actionExportexcel(){
		$exportdata = array();
		$titles = array ('imei' => 'IMEI', 'statusid' => 'STATUS ID', 'planid' => 'PLAN ID', 'updatetypeid' => 'UPDATE TYPE ID', 'particulars' => 'PARTICULARS', 'updateon' => 'UPDATE ON', 'datetime' => 'DATETIME', 'userid' => 'USER ID', 'ratified' => 'RATIFIED', 'comments' => 'COMMENTS'); 
		array_push($exportdata, $titles);
		$imeidata = (new \yii\db\Query())
						->select(['imei', 'statusid', 'planid', 'updatetypeid', 'particulars', 'updateon', 'datetime', 'userid', 'ratified', 'comments'])
						->from('devicestatus_spytec.updates')
						->where(['ratified' => 0])
						->all();
						
		for($i = 0; $i < count($imeidata); $i++){
			array_push($exportdata, $imeidata[$i]);	
		}			
	
		ini_set('max_execution_time', 900);
		ini_set('memory_limit', '-1');
		
		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set document properties
		$objPHPExcel->getProperties()->setCreator("Bhaskar Bhattachariya")
							 ->setLastModifiedBy("Bhaskar Bhattachariya")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("php data export result file");
		
		$cellarray = explode(',', "A,B,C,D,E,F,G,H,I,J,K,L,K,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z");  //please increase the number here if you need
		$counter = 1; // since 1 based		
		$objPHPExcel->setActiveSheetIndex(0);		
		foreach ($exportdata as $key => $value) {
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit("A".$counter, $value['imei'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("B".$counter, $value['statusid'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("C".$counter, $value['planid'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("D".$counter, $value['updatetypeid'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("E".$counter, $value['particulars'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("F".$counter, $value['updateon'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("G".$counter, $value['datetime'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("H".$counter, $value['userid'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("I".$counter, $value['ratified'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("J".$counter, $value['comments'], PHPExcel_Cell_DataType::TYPE_STRING);

			$counter++;
		}
		
		// Rename worksheet
		$objPHPExcel->getActiveSheet()->setTitle('Update list');
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);		
		
		// Redirect output to a client’s web browser (OpenDocument)
		header('Content-Type: application/vnd.oasis.opendocument.spreadsheet');
		header('Content-Disposition: attachment;filename="imei1.ods"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		//header('Cache-Control: max-age=1');
		
		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 2016 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0
		
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'OpenDocument');
		$objWriter->save('php://output');
	}
	
		public function actionExportupdaterange(){
			$exportdata = array();
			$dates = array();
			$titles = array ('imei' => 'IMEI', 'date' => 'DATE', 'updateon' => 'UPDATE ON', 'updatetypeid' => 'UPDATE TYPE ID', 'oldstatus' => 'OLD STATUS', 'newstatus' => 'NEW STATUS', 'oldparticulars' => 'OLD PARTICULARS', 'newparticulars' => 'NEW PARTICULARS', 'comments' => 'COMMENTS', 'username' => 'USERNAME'); 
			array_push($exportdata, $titles);
			if(count(Yii::$app->request->queryParams) == 2){
				$dates = explode(" ",Yii::$app->request->queryParams['HistorySearch']['date']); 
				$start_date = $dates[0];
				$end_date = $dates[2];
				$exportdata = (new \yii\db\Query())
								->select(['imei', 'date', 'updateon', 'updatetypeid', 'oldstatus', 'newstatus', 'oldparticulars', 'newparticulars', 'comments', 'username'])
								->from('devicestatus_spytec.history')
								->where(['>=', 'date', $start_date])
								->andWhere(['<=', 'date', $end_date])
								->all();
			} else {
					$exportdata = (new \yii\db\Query())
								->select(['imei', 'date', 'updateon', 'updatetypeid', 'oldstatus', 'newstatus', 'oldparticulars', 'newparticulars', 'comments', 'username'])
								->from('devicestatus_spytec.history')
								->where(['like', 'date', date('Y-m-d')])
								->all();
			}			
		
			ini_set('max_execution_time', 900);
			ini_set('memory_limit', '-1');
		
			// Create new PHPExcel object
			$objPHPExcel = new PHPExcel();

			// Set document properties
			$objPHPExcel->getProperties()->setCreator("Bhaskar Bhattachariya")
							 ->setLastModifiedBy("Bhaskar Bhattachariya")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("php data export result file");
		
			$cellarray = explode(',', "A,B,C,D,E,F,G,H,I,J,K,L,K,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z");  //please increase the number here if you need
			$counter = 1; // since 1 based		
			$objPHPExcel->setActiveSheetIndex(0);		
			foreach ($exportdata as $key => $value) {
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit("A".$counter, $value['imei'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("B".$counter, $value['date'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("C".$counter, $value['updateon'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("D".$counter, $value['updatetypeid'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("E".$counter, $value['oldstatus'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("F".$counter, $value['newstatus'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("G".$counter, $value['oldparticulars'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("H".$counter, $value['newparticulars'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("I".$counter, $value['comments'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("J".$counter, $value['username'], PHPExcel_Cell_DataType::TYPE_STRING);
				$counter++;
			}
		
			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('Update History');
		
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);		
		
			// Redirect output to a client’s web browser (OpenDocument)
			header('Content-Type: application/vnd.oasis.opendocument.spreadsheet');
			header('Content-Disposition: attachment;filename="updatehistory.ods"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			//header('Cache-Control: max-age=1');
		
			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 2016 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0
		
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'OpenDocument');
			$objWriter->save('php://output'); 
		}
	
		public function actionExportupdate(){
			$exportdata = array();
			$titles = array ('imei' => 'IMEI', 'date' => 'DATE', 'updateon' => 'UPDATE ON', 'updatetypeid' => 'UPDATE TYPE ID', 'oldstatus' => 'OLD STATUS', 'newstatus' => 'NEW STATUS', 'oldparticulars' => 'OLD PARTICULARS', 'newparticulars' => 'NEW PARTICULARS', 'comments' => 'COMMENTS', 'username' => 'USERNAME'); 
			array_push($exportdata, $titles);
			
			// print_r(Yii::$app->request->queryParams);
			$imei = Yii::$app->request->queryParams['imei']; 
			$date = Yii::$app->request->queryParams['date'];
			if($date != ''){
				$date_arr = explode(" ", $date);	
				$start_date = $date_arr[0];
				$end_date = $date_arr[2];
			}
			$updateon = Yii::$app->request->queryParams['updateon'];
			if($updateon != ''){
				$update_arr = explode(" ", $updateon);	
				$start_update_date = $update_arr[0];
				$end_update_date = $update_arr[2];
			}
			$updatetypeid = Yii::$app->request->queryParams['updatetypeid']; 
			$oldstatus = Yii::$app->request->queryParams['oldstatus'];
			$newstatus = Yii::$app->request->queryParams['newstatus'];
			$oldplan = Yii::$app->request->queryParams['oldplan'];
			$newplan = Yii::$app->request->queryParams['newplan'];
			$oldparticulars = Yii::$app->request->queryParams['oldparticulars'];
			$newparticulars = Yii::$app->request->queryParams['newparticulars'];
			$comments = Yii::$app->request->queryParams['comments'];
			$username = Yii::$app->request->queryParams['username'];
				
			/* $exportdata = (new \yii\db\Query())
						->select(['imei', 'date', 'updateon', 'updatetypeid', 'oldstatus', 'newstatus', 'oldparticulars', 'newparticulars', 'comments', 'username'])
						->from('devicestatus_spytec.history')
						->where(['like', 'imei', $imei])
						->andWhere(['like', 'start_date', $start_date])
						->andWhere(['like', 'end_date', $end_date])
						->all(); */
						
			$connection = Yii::$app->getDb();
			$select = "SELECT imei, date, updateon, updatetypeid, oldstatus, newstatus, oldparticulars, newparticulars, comments, username";
			$where = "";
			$firstWhere = true;
			$whereKeyword = " WHERE";
			$andKeyword = " AND";
			$keyword = $whereKeyword;
			if($imei != ''){
				if(!$firstWhere){
					$keyword = $andKeyword;
				}
				$where .= $keyword." imei = ".$imei;
				$firstWhere = false;
			} 
			if($date != ''){
				if(!$firstWhere){
					$keyword = $andKeyword;
				}
				$where .= $keyword." date >= ".$start_date." AND date <= ".$end_date;
				$firstWhere = false;
			} 
			if($updateon != ""){
				if(!$firstWhere){
					$keyword = $andKeyword;
				}
				$where .= $keyword." updateon = ".$updateon;
				$firstWhere = false;
			} 
			if($updatetypeid != ""){
				if(!$firstWhere){
					$keyword = $andKeyword;
				}
				$where .= $keyword." updatetypeid = ".$updatetypeid;
				$firstWhere = false;
			}
			if($oldstatus != ""){
				if(!$firstWhere){
					$keyword = $andKeyword;
				}
				$where .= $keyword." oldstatus = ".$oldstatus;
				$firstWhere = false;
			}
			if($newstatus != ""){
				if(!$firstWhere){
					$keyword = $andKeyword;
				}
				$where .= $keyword." newstatus = ".$newstatus;
				$firstWhere = false;
			}
			if($oldparticulars != ""){
				if(!$firstWhere){
					$keyword = $andKeyword;
				}
				$where .= $keyword." oldparticulars = ".$oldparticulars;
				$firstWhere = false;
			}
			if($newparticulars != ""){
				if(!$firstWhere){
					$keyword = $andKeyword;
				}
				$where .= $keyword." newparticulars = ".$newparticulars;
				$firstWhere = false;
			}
			if($comments != ""){
				if(!$firstWhere){
					$keyword = $andKeyword;
				}
				$where .= $keyword." comments = ".$comments;
				$firstWhere = false;
			}
			if($username != ""){
				if(!$firstWhere){
					$keyword = $andKeyword;
				}
				$where .= $keyword." username = ".$username;
				$firstWhere = false;
			}
			$from = " FROM devicestatus_spytec.history";
			//$command = $connection->createCommand($query);
			//$result = $command->queryAll();
			print_r($select.$where.$from);	
			
			/* ini_set('max_execution_time', 900);
			ini_set('memory_limit', '-1');
		
			// Create new PHPExcel object
			$objPHPExcel = new PHPExcel();

			// Set document properties
			$objPHPExcel->getProperties()->setCreator("Bhaskar Bhattachariya")
							 ->setLastModifiedBy("Bhaskar Bhattachariya")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("php data export result file");
		
			$cellarray = explode(',', "A,B,C,D,E,F,G,H,I,J,K,L,K,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z");  //please increase the number here if you need
			$counter = 1; // since 1 based		
			$objPHPExcel->setActiveSheetIndex(0);		
			foreach ($exportdata as $key => $value) {
				$objPHPExcel->setActiveSheetIndex(0)
				->setCellValueExplicit("A".$counter, $value['imei'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("B".$counter, $value['date'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("C".$counter, $value['updateon'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("D".$counter, $value['updatetypeid'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("E".$counter, $value['oldstatus'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("F".$counter, $value['newstatus'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("G".$counter, $value['oldparticulars'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("H".$counter, $value['newparticulars'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("I".$counter, $value['comments'], PHPExcel_Cell_DataType::TYPE_STRING)
				->setCellValueExplicit("J".$counter, $value['username'], PHPExcel_Cell_DataType::TYPE_STRING);
				$counter++;
			}
		
			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('Update History');
		
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);		
		
			// Redirect output to a client’s web browser (OpenDocument)
			header('Content-Type: application/vnd.oasis.opendocument.spreadsheet');
			header('Content-Disposition: attachment;filename="updatehistory.ods"');
			header('Cache-Control: max-age=0');
			// If you're serving to IE 9, then the following may be needed
			//header('Cache-Control: max-age=1');
		
			// If you're serving to IE over SSL, then the following may be needed
			header ('Expires: Mon, 26 Jul 2016 05:00:00 GMT'); // Date in the past
			header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
			header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
			header ('Pragma: public'); // HTTP/1.0
		
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'OpenDocument');
			$objWriter->save('php://output'); */
		}
	
	public function actionSummary(){
		
		$totaldevices = Imeidata::find()->count();
		$totalunused = Unused::find()->count();
				
		/*$statuswiseImei= array();
		$items = ArrayHelper::map(Status::find()->all(), 'id', 'status');
		foreach ($items as $key => $value) {
			$statuswiseImei[$value] = Imeidata::find()->where(['status'=>$value])->count();
		}
		
		$planwiseImei = array();
		$items = ArrayHelper::map(Plans::find()->all(), 'planid', 'plan');
		foreach ($items as $key => $value) {
			$planwiseImei[$value] = Imeidata::find()->where(['planid'=>$key])->count();
		}
		*/
		$variable = Imeidata::find()->select(['imei', 'planid'])->where(['status'=>'Active'])->asArray()->all();
		
		$aimei = array();
		$arr_activeplan= array();
		foreach ($variable as $key => $value) {
			array_push($aimei, $value['imei']);
			$arr_activeplan[$value['imei']] = $value['planid'];
		}
			
		//$variable2 = Unused::find()->select('imei')->asArray()->all();
		//$bimei = array();
		//foreach ($variable2 as $key => $value) {
		//	array_push($bimei, $value['imei']);
		//}
				
		//$diffarray = array_diff($aimei, $bimei);
		
		$count_arr=array();
		$items = ArrayHelper::map(Plans::find()->all(), 'planid', 'plan');
		foreach ($items as $key => $value) {
			$count_arr[$key] = array('name'=>$value, 'count'=>0);
		}
		$deviceswithnullplan = array();
		//foreach ($diffarray as $value) {
		foreach ($aimei as $value){			
			$planid =  $arr_activeplan[$value];
			if(is_null($arr_activeplan[$value])){
				array_push($deviceswithnullplan, $value); 
				continue;
			}
			$count_arr[$planid]['count']++;
		}
		return $this->render('/company/summary', [
                'totaldevices' => $totaldevices,
               // 'statuswiseImei'=>$statuswiseImei,
               // 'planwiseImei'=>$planwiseImei,
                'totalunused'=>$totalunused,
                //'usedactivemin'=>count($diffarray),
                'cname'=>$this->companyname,
                'plancount'=>$count_arr,
                'issuedevices'=>$deviceswithnullplan,
            ]);
	}

    public function actionReturnsim(){
        
        $imeis = Imeidata::find()->where(['sim'=>NULL])->andWhere(['phone'=>NULL])->asArray()->all();
        $count = 0;
        foreach ($imeis as $key => $value) {
            //print_r($value['imei']);
            $history = ChangeHistory::find()->where(['imei'=>$value['imei']])->orderBy(['id'=> SORT_DESC])->one();
            $pushsim=Imeidata::find()->where(['imei'=>$value['imei']])->one();
            $pushsim->sim=$history->oldsim;
            $pushsim->phone=$history->oldphone;
            
            $pushsim->save();
            
            $count++;
        }
        
        return $count;
    }
}
