<?php

namespace app\controllers;

class DataController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionUpload()
    {
        return $this->render('upload');
    }

    public function actionView()
    {
        return $this->render('view');
    }

}
