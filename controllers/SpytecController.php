<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\controllers\CompanyController;

use yii\filters\AccessControl;
/**
 * UserController implements the CRUD actions for Users model.
 */
class SpytecController extends CompanyController
{

    public $companyname = "spytec";
}
