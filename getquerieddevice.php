<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

$this->title = 'Status of Queried device';
/* @var $this yii\web\View */
/* @var $model app\models\Updates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="updates-create">
	<?php $form = ActiveForm::begin([
		'action' => 'getquerieddevice'
	]); ?>

    <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
		    'options' => ['placeholder' => 'Select date..'],
		    'pluginOptions' => [
		        'format' => 'yyyy-mm-dd',
		        'todayHighlight' => true
		    ]
		]) ?>
		
	<?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'value'=>'submit', 'name'=>'submit']) ?>	

    <?php ActiveForm::end(); ?>

</div>